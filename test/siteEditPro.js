describe("My Name Is", function() {  //название теста
	it('should return my name', function() { // функция должна вернуть моё имя
		expect(myNameIs()).toEqual("Nick");  // ожидается функция myNameIs() , результат которой равен строке "Nick"
	});
});