<?

class Fileupload{

	public function __construct(){
		$this->upload();
	}

	protected  function upload(){
		$name_of_uploaded_file = basename($_FILES['uploaded_file']['name']);
		$type_of_uploaded_file = substr($name_of_uploaded_file, strrpos($name_of_uploaded_file, '.') + 1);
		$size_of_uploaded_file = $_FILES["uploaded_file"]["size"]/1024;


		$max_allowed_file_size = 1024;
		$allowed_extensions = array("jpg","jpeg","gif","bmp");
		$errors = '';
		if($size_of_uploaded_file > $max_allowed_file_size){
			$errors .= "\n Размер файла должен быть меньше $max_allowed_file_size";
		}

		$allowed_ext = false;
		for($i = 0; $i < sizeof($allowed_extensions); $i++){
			if(strcasecmp($allowed_extensions[$i],$type_of_uploaded_file) == 0){
				$allowed_ext = true;
			}
		}

		if(!$allowed_ext){
			$errors .= "\n Расширение файла не соответствует требуемому. "."Поддерживаются следующие расширения: ".implode(',',$allowed_extensions);
		}



		$path_of_uploaded_file = 'uploads/'.$name_of_uploaded_file;

		$tmp_path = $_FILES["uploaded_file"]["tmp_name"];

		if(is_uploaded_file($tmp_path)){
			if(!copy($tmp_path,$path_of_uploaded_file)){
				$errors .= '\n error while copying the uploaded file';
			}
		}
	}
}