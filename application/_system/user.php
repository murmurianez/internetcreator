<?

class User {
	private $db;

	function __construct(){
		$this->db = new Db();
	}

	function addUser($email,$pass,$role){
		if(empty($role)){
			$role = 'user';
		}

		$query = $this->db->dbConnect->prepare("INSERT INTO users (u_email, u_password, u_role) VALUES (:email, :pass, :role)");
		$query->bindParam(':email',$email);
		$query->bindParam(':pass',$pass);
		$query->bindParam(':role',$role);
		$query->execute();

		ChromePhp::log('addUser');
	}

	function updateUser($id){
		$query = $this->db->dbConnect->prepare("UPDATE INTO users (u_id_user, u_name, u_lastname, u_role) VALUES (:id, :name, :lastname, :role)");
		$query->bindParam(':id',$id);
		$query->bindParam(':name',$name);
		$query->bindParam(':lastname',$lastname);
		$query->bindParam(':role',$role);
		$query->execute();
	}

	function deleteUser($id){
		$query = $this->db->dbConnect->prepare("DELETE * FROM users WHERE u_id_user=:id LIMIT 1");
		$query->bindParam(':id',$id);
		$query->execute();
	}

	function getInfo($id){
		$result = $this->db->dbConnect->query("SELECT * FROM users WHERE u_id_user=$id LIMIT 1");
		return $result->fetch(PDO::FETCH_OBJ);
	}
}