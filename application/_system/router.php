<?

class Router{
	function __construct(){

		if($_SERVER['QUERY_STRING'] != ''){
			$page = $_SERVER['QUERY_STRING'];
		}else{
			$page = 'mainpage';
		}

		$args = explode('/',$_SERVER['QUERY_STRING']);

		require_once("application/blocks/header/model/header.php");
		require_once('application/pages/'.$page.'/model/'.$page.'.php');
		$class = ucfirst($page);
		$loadPage = new $class($args);
		require_once("application/blocks/footer/model/footer.php");
	}
}