<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/static/css/main.css">
	<link rel="stylesheet" type="text/css" href="/static/css/<?=$page?>.css">
	<link rel="stylesheet" type="text/css" href="/static/css/append.css">
	<link rel="stylesheet" type="text/css" href="/static/css/tooltip.css">
	<link rel="stylesheet" type="text/css" href="/static/css/popup.css">
	<link href='http://fonts.googleapis.com/css?family=Poiret+One|Philosopher|Yeseva+One|Tenor+Sans|Oranienbaum|Lobster|Open+Sans:800|Roboto:400,100|Roboto+Condensed:300,400|Forum|Prosto+One|Marmelad&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

	<? if($_SERVER['SERVER_ADDR'] == '192.168.0.10'){echo '<script src="/static/js/common/_develop/console.js"></script>';} ?>

	<link rel="prerender" href="http://internetcreator/?siteeditpro">

	<script src="/static/js/foreign/jquery-2.0.min.js"></script>
</head>
<body id="body" class="unselectable">
	<div id="top">
		<div class="top-menu">
			<div class="logo"><a href="?mainpage">LayerStreet</a></div>

			<!--<div class="banner">-->
				<!--<div class="content">-->
					<!--<a href="https://www.roi.ru/poll/petition/gosudarstvennoe_upravlenie1/zapret-chinovnikam-i-sotrudnikam-kompanij-s-gosudarstvennym-munitcipalnym-uchastiem-priobetat-legkovye-avtomobili-stoimostyu-svyshe-15-millionov-rublej/" target="_blank">-->
						<!--<div class="img"><img src="/static/img/sprites/roi.png" /></div>-->
						<!--<span>Голосуй против покупки<br />чиновниками автомобилей<br /> дороже 1,5 млн. рублей!</span>-->
					<!--</a>-->
				<!--</div>-->
			<!--</div>-->

			<div class="top-button-block">
				<div class="button">
                    <a href="javascript:" style="min-width:180px; text-align:center;" class="save-button" onmouseover="$(this).text('Сохранить')" onmouseout="$(this).text('Сохранено 3 минуты назад')" onclick="content.save()">Сохранено 3 минуты назад</a>
                </div>
			</div>

			<div class="user login-registration">
				<div class="content">
					<a href="javascript:">
						<span>Login / Registration</span>
					</a>
				</div>
			</div>

			<div class="user">
				<div class="content">
					<a href="?userpage">
						<div class="img"><img src="/static/img/ava.jpg" /></div>
						<span>Алексей Андреев</span>
					</a>
				</div>
			</div>

		</div>
	</div>

<noscript>
	<div>
		Для работы сервиса, Вам необходимо включить JavaScript
	</div>
</noscript>