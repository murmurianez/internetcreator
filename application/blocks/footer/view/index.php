        <div style="float:left; width:100%;">
			<div style="width:940px; height:40px; padding-left:20px; margin:0 auto; line-height:40px; background-color:#EEEEEE;">

				<? if($_SERVER['SERVER_ADDR'] == '192.168.0.10'){?>
					<a href="?test">Тестовая страница</a>
					<a href="?constitution">Конституция</a>
				<?}?>

				<a href="?develop">Для разработчиков</a>
			</div>
        </div>

		<script src="/static/js/foreign/lodash-1.3.1.js"></script>
		<script data-main="/static/js/app" src="/static/js/requirejs-2.1.6.js"></script>
		<script src="/static/js/main.min.js"></script>

		<? if($_SERVER['REQUEST_URI'] == "/?siteeditpro"){?>
			<script src="/static/js/siteeditpro.min.js"></script>
		<?}?>

		<? if($_SERVER['REQUEST_URI'] == "/?userpage"){?>
			<script src="/static/js/userpage.min.js"></script>
		<?}?>

		<script src="/static/js/foreign/statistics.js"></script>
	</body>
</html>