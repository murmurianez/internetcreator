var library = {

//Копирование объекта с разрывом связей (каждый живёт своей жизнью)
	copy: function(obj){
		if(!obj || typeof obj !== 'object'){
			return obj;
		}
		var c = (typeof obj.pop === 'function') ? [] : {};
		var p, v;
		for(p in obj){
			if(obj.hasOwnProperty(p)){
				v = obj[p];
				if(v && typeof v === 'object'){
					c[p] = library.clone(v);
				}else{
					c[p] = v;
				}
			}
		}
		return c;
	}



//	function wheel(event){
//	var delta = 0;
//	if (!event) event = window.event; // Событие IE.
//	// Установим кроссбраузерную delta
//	if (event.wheelDelta) {
//		// IE, Opera, safari, chrome - кратность дельта равна 120
//		delta = event.wheelDelta/120;
//	} else if (event.detail) {
//		// Mozilla, кратность дельта равна 3
//		delta = -event.detail/3;
//	}
//	if (delta) {
//		// Отменим текущее событие - событие поумолчанию (скролинг окна).
//		if (event.preventDefault) {
//			event.preventDefault();
//		}
//		event.returnValue = false; // для IE
//
//		// если дельта больше 0, то колесо крутят вверх, иначе вниз
//		var dir = delta > 0 ? 'Up' : 'Down',
//			}
//	}



//	var mouseWheel = {
//		result:0,
//		MinSize:-5,
//		MaxSize:5,
//		Step:2,
//
//		hookEvent:function(hElem, eventName, callback){
//			if(typeof(hElem) == 'string'){
//				hElem = document.getElementById(hElem);
//			}
//
//			if(!hElem){
//				return false;
//			}
//
//			if(hElem.addEventListener){
//				if(eventName == 'mousewheel'){
//					hElem.addEventListener('DOMMouseScroll', callback, false);
//				}
//				hElem.addEventListener(eventName, callback, false);
//			}else{
//				if(hElem.attachEvent){
//					hElem.attachEvent('on' + eventName, callback);
//				}else{
//					return false;
//				}
//			}
//			return true;
//		},
//
//		unhookEvent:function(hElem, eventName, callback){
//			if(typeof(hElem) == 'string'){
//				hElem = document.getElementById(hElem);
//			}
//			if(!hElem){
//				return false;
//			}
//
//			if(hElem.removeEventListener){
//				if(eventName == 'mousewheel'){
//					hElem.removeEventListener('DOMMouseScroll', callback, false);
//				}
//				hElem.removeEventListener(eventName, callback, false);
//			}else{
//				if(hElem.detachEvent) {
//					hElem.detachEvent('on' + eventName, callback);
//				}else{
//					return false;
//				}
//			}
//			return true;
//		},
//
//		cancelEvent:function(e){
//			e = e ? e : window.event;
//
//			if(e.stopPropagation){
//				e.stopPropagation();
//			}
//
//			if(e.preventDefault){
//				e.preventDefault();
//			}
//
//			e.cancelBubble = true;
//			e.cancel = true;
//			e.returnValue = false;
//			return false;
//		},
//
//		mouseWheelFunction:function(e){
//			e = e ? e : window.event;
//
//			var wheelElem = e.target ? e.target : e.srcElement;
//			var wheelData = e.detail ? e.detail * -1 : e.wheelDelta / 40;
//
//			if(Math.abs(wheelData) > 100){
//				wheelData = Math.round(wheelData / 100);
//			}
//
//			var nw = wheelElem.offsetWidth;
//			var nh = wheelElem.offsetHeight;
//			var coeff = nw !== 0 ? (nh / nw) : 1;
//			var delta = wheelData * mouseWheel.Step;
//			nw += delta;
//			nh += Math.round(delta * coeff);
//
//			if(nw > mouseWheel.MinSize && nh > mouseWheel.MinSize && nw < mouseWheel.MaxSize && nh < mouseWheel.MaxSize){
//				wheelElem.style.width = nw + 'px';
//				wheelElem.style.height = nh + 'px';
//			}
//
//			return mouseWheel.cancelEvent(e);
//		},
//
//		setHook:function(obj,act){
//			if(act){
//				mouseWheel.hookEvent(obj.id, 'mousewheel', mouseWheel.mouseWheelFunction)
//			}else{
//				mouseWheel.unhookEvent(obj.id, 'mousewheel', mouseWheel.mouseWheelFunction);
//			}
//		}
//	};





//    //Наследование объекта
//    inherit: function(p){
//        if(p == null){throw TypeError()}
//        if(Object.create){return Object.create(p)}
//
//        var t = typeof p;
//        if(t !== "object" && t !== "function"){throw TypeError()}
//        function f(){}
//        f.prototype = p;
//        return new f();
//    },
////
//    //Копирование свойств из второго объекта в первый с заменой
//    extend: function(o,p){
//        for(var prop in p){
//            o[prop] = p[prop];
//        }
//        return o;
//    },
//
//
//    //Копирование свойств из одного объекта в другой без замены
//    merge: function(o,p){
//        for(prop in p){
//            if(o.hasOwnProperty[prop]){continue};
//            o[prop] = p[prop];
//        }
//    },
//
//    //Удаление из первого объекта свойств отсутствующих во втором
//    restrict: function(o,p){
//        for(prop in o){
//            if(!(prop in p)){
//                delete o[prop];
//            }
//        }
//        return o;
//    },
//
//    //Удаление из первого объекта свойств присутствующих во втором
//    subtract: function(o,p){
//        for(prop in p){
//            delete o[prop];
//        }
//    },
//
//    //Возвращает новый объект со свойствами обоих объектов. Если свойства совпадают - предпочтение отдаётся свойствам второго объекта
//    union: function(o,p){
//        return library.extend(library.extend({},o),p);
//    },
//
//    //Возвращает объект со свойсвами присутствующими в обоих объектах. Если свойства совпадают - предпочтение отдаётся свойствам второго объекта
//    intersection: function(o,p){
//        return library.restrict(library.extend({},o),p);
//    },
//
//    //Возвращение массива с именами свойств объекта
//    keys: function(o){
//        if(typeof o !== "object"){throw TypeError()}
//        if(Object.keys){return Object.keys(o)}
//        var result = [];
//        for(var prop in o){
//            if(o.hasOwnProperty(prop)){
//                result.push(prop);
//            }
//        };
//        return result;
//    }
};
console.info('common/library');