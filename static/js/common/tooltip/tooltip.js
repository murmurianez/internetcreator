var Tooltip = function(){
	this.elem = '';
	this.width = 300;
	this.height = 200;
	this.top = function(){return this.elem.offset().top + 37;};
	this.left = function(){return this.elem.offset().left;};
	this.autorun = function(){};

	this.content = '';
	this.buttons = 	'';

	this.show = function(elem){
		this.elem = elem;
		glob.BODY.append('' +
			'<div class="tooltip" style="left:' + this.left() + 'px; top:' + this.top() + 'px; width:' + this.width + 'px; height:' + this.height + 'px;">' +
				'<div class="content">' + this.content + '</div>' +
			'</div>');
		this.autorun();
	};
	this.hide = function(){$('.tooltip').remove();};
};






var PaletteTooltip = function(){
	this.width = 200;
	this.top = function(){return this.elem.offset().top;};
	this.left = function(){return this.elem.offset().left;};
	this.content = 	'<div class="backgrounds"></div>' +
					'<div>' +
						'<a href="javascript:" class="color-picker-show cyan-button">Настртойка палитры</a>' +
					'</div>';
	this.autorun = function(){
		var backgrounds = '';
		for(var i = settings.colorPalette.length; i--;){
			backgrounds = backgrounds + '<div class="background" data-value="div_css_background" style="float:left; width:50px; height:50px; background:' +settings.colorPalette[i]+ '"></div>';
		}

		$('.tooltip .content .backgrounds').append(backgrounds);
	}
};
PaletteTooltip.prototype = new Tooltip();
var paletteTooltip = new PaletteTooltip();