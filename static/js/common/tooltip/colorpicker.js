var colorPicker = {
	element:'',
	top:0,
	left:0,
	resultValue:'',
	show:function(element,x,y,val){
		colorPicker.top = y + 37;
		colorPicker.left = x;
		colorPicker.element = element;

		var content = 	'<div id="color-picker" style="left:' + colorPicker.left + 'px; top:' +colorPicker.top+ 'px;">' +
							'<div class="color-changer">' +
								'<div class="close-popup cyan-button">Закрыть</div>' +
								'<div class="ok-popup cyan-button">Применить</div>' +
								'<div class="input">RGB: <input type="text" class="rgb" /></div>' +
								'<div class="input">Direction: <input type="number" class="direction" min="0" max="359" value="0" /></div>' +
								'<div class="picker">' +
									'<canvas id="picker" width="150" height="150"></canvas>' +
									'<canvas id="colors" width="20" height="150"></canvas>' +
									'<div class="change-colors">' +
										'<div class="color-picker"></div>' +
										'<div class="current"></div>' +
									'</div>' +
									'<div class="add-color">' +
										'<div class="line"></div>' +
										'<div class="add-button">' +
											'<i class="icon icon-black icon-a"></i>' +
											'<div class="triangle"></div>' +
										'</div>' +
									'</div>' +
									'<div class="transparent"><div class="gradient"></div></div>' +
									'<div class="transparent"><div class="result"></div></div>' +
								'</div>' +
								'<div class="text-color">' +
									'<div>R: <input type="number" class="rrgb" min="0" max="255" /></div>' +
									'<div>G: <input type="number" class="grgb" min="0" max="255" /></div>' +
									'<div>B: <input type="number" class="brgb" min="0" max="255" /></div>' +
									'<div>A: <input type="number" class="argb" min="0" max="1" value="1" /></div>' +
									'<div><input type="range" class="opacity" min="0" max="100" value="100" /></div>' +
								'</div>' +
							'</div>' +
							'<div class="sets">' +
								'<div class="colors-sets transparent">' +
									'<div class="color-set">' +
										'<div style="background-color:#FC4639"></div>' +
										'<div style="background-color:#FFD541"></div>' +
										'<div style="background-color:#22B4AA"></div>' +
										'<div style="background-color:#006774"></div>' +
										'<div style="background-color:#391A22"></div>' +
									'</div>' +
									'<div class="color-set">' +
										'<div style="background-color:#91785B"></div>' +

									'</div>' +
								'</div>' +
								'<div class="gradients transparent">' +
									'<div style="background:-webkit-linear-gradient(top, rgba(169,3,41,1) 0%,rgba(143,2,34,1) 44%,rgba(109,0,25,1) 100%);"></div>' +
									'<div style="background:-webkit-linear-gradient(top, rgba(213,206,166,1) 0%,rgba(201,193,144,1) 40%,rgba(183,173,112,1) 100%);"></div>' +
									'<div style="background:-webkit-linear-gradient(top, rgba(98,125,77,1) 0%,rgba(31,59,8,1) 100%);"></div>' +
									'<div style="background:-webkit-linear-gradient(top, rgba(30,87,153,1) 0%,rgba(41,137,216,1) 50%,rgba(32,124,202,1) 51%,rgba(125,185,232,1) 100%);"></div>' +
									'<div style="background:-webkit-linear-gradient(top, rgba(210,255,82,1) 0%,rgba(145,232,66,1) 100%);"></div>' +
									'<div style="background:-webkit-linear-gradient(top, rgba(30,87,153,1) 0%,rgba(89,148,202,1) 62%,rgba(95,154,207,0.7) 68%,rgba(125,185,232,0) 100%);"></div>' +
									'<div style="background:-webkit-linear-gradient(top, rgba(216,224,222,1) 0%,rgba(174,191,188,1) 22%,rgba(153,175,171,1) 33%,rgba(142,166,162,1) 50%,rgba(130,157,152,1) 67%,rgba(78,92,90,1) 82%,rgba(14,14,14,1) 100%);"></div>' +
									'<div style="background:-webkit-gradient(linear, left top, left bottom, from(#a59179), to(#91785b))"></div>' +
								'</div>' +
								'<div class="textures transparent">' +
									'<div style="background-image:url(/static/img/textures/brown.png)"></div>' +
									'<div style="background-image:url(/static/img/textures/black.jpg)"></div>' +
									'<div style="background-image:url(/static/img/textures/gray.png)"></div>' +
									'<div style="background-image:url(/static/img/textures/crissXcross.png)"></div>' +
									'<div style="background-image:url(/static/img/textures/dark_mosaic.png)"></div>' +
									'<div style="background-image:url(/static/img/textures/tactile_noise.png)"></div>' +
								'</div>' +
							'</div>' +
						'</div>';

		$('body').append(content);
		colorPicker.init();
		$('.previous').css('background-color',val);
	},

	hide:function(elem){
		elem.parent().hide();
	},

	init:function(){
		var canvas = document.getElementById('picker');
		var ctx = canvas.getContext('2d');
		var img = new Image();
		img.src = '/static/img/system/colorpicker_overlay.png';
		img.onload = function(){
			ctx.fillStyle = 'rgb(255,0,0)';
			ctx.fillRect(0,0,150,150);
			ctx.drawImage(img,0,0);
		};

		canvas.onmousemove = function(e){
			var mouseX, mouseY;
			if(e.offsetX) {
				mouseX = e.offsetX;
				mouseY = e.offsetY;
			}
			else if(e.layerX) {
				mouseX = e.layerX;
				mouseY = e.layerY;
			}
			var c = ctx.getImageData(mouseX, mouseY, 1, 1).data;
			$('.rrgb').val(c[0]);
			$('.grgb').val(c[1]);
			$('.brgb').val(c[2]);

			var o = $('.opacity').val()/100;
			$('.current').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
		};
		canvas.onmousedown = function(e){
			var mouseX, mouseY;
			if(e.offsetX) {
				mouseX = e.offsetX;
				mouseY = e.offsetY;
			}
			else if(e.layerX) {
				mouseX = e.layerX;
				mouseY = e.layerY;
			}
			var c = ctx.getImageData(mouseX, mouseY, 1, 1).data;
			$('.rrgb').val(c[0]);
			$('.grgb').val(c[1]);
			$('.brgb').val(c[2]);
			var o = $('.opacity').val()/100;
			$('.color-picker').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
		};



		var colors = document.getElementById('colors');
		var crs = colors.getContext('2d');
		var gradient = new Image();
		gradient.src = '/static/img/system/colorpicker_gradient.png';
		gradient.onload = function(){
			crs.drawImage(gradient,0,0,20,150);
		};
		colors.onmousemove = function(e){
			var mouseX, mouseY;
			if(e.offsetX) {
				mouseX = e.offsetX;
				mouseY = e.offsetY;
			}
			else if(e.layerX) {
				mouseX = e.layerX;
				mouseY = e.layerY;
			}
			var c = crs.getImageData(mouseX, mouseY, 1, 1).data;
			var o = $('.opacity').val()/100;
			$('.current').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
		};
		colors.onmousedown = function(e){
			var mouseX, mouseY;
			if(e.offsetX) {
				mouseX = e.offsetX;
				mouseY = e.offsetY;
			}
			else if(e.layerX) {
				mouseX = e.layerX;
				mouseY = e.layerY;
			}
			var c = crs.getImageData(mouseX, mouseY, 1, 1).data;
			ctx.fillStyle = 'rgb(' + c[0] + ',' + c[1] + ',' + c[2] + ')';
			ctx.fillRect(0,0,150,150);
			ctx.drawImage(img,0,0);
			var o = $('.opacity').val()/100;
			$('.color-picker').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
		};
	},

	result:function(){
		colorPicker.element.val(colorPicker.resultValue);
		colorPicker.element.change();
	}
};



var gradient = {
	colorNum:0,
	addColors:function(){
		$('.gradient').append('<div class="color"><input type="checkbox" /><div class="gradient-color i' + gradient.colorNum + '"></div></div>');
		$('.gradient-color.i' + gradient.colorNum).css('background-color',function(){
			return $('.color-picker').css('background-color');
		});
		gradient.colorNum = gradient.colorNum + 1;
		gradient.colors();
	},

	colors:function(){
		$('.result').css('background',function(){
			var direction = $('.direction').val() + 'deg';
			var color = '';

			for(var i = 0; i <= gradient.colorNum-1; i++){
				var thisColor = $('.gradient-color.i' + i).css('background-color');
				var k = i*(100/gradient.colorNum)|0;
				color = color + ', ' + thisColor + ' ' + k + '%';
			}
			colorPicker.resultValue = '-webkit-linear-gradient('+ direction + color + ')';
			return '-webkit-linear-gradient('+ direction + color + ')';
		});
	}
};