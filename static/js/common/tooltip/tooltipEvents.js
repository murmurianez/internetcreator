var tooltipEvents = {
	events:function(eventsOn){
		if(eventsOn){
			//paletteTooltip --------------------------------------
			$('body').on('mouseover','.panel-box .palette',function(){
				$('.palette').css({'height':'100%','border':'1px solid #B9BEC3','box-shadow':'0 1px 10px rgba(0,0,0,.2)'});
			});
			$('body').on('mouseout','.panel-box .palette',function(){
				$('.palette').css({'height':'50px','border':'1px solid #F1F1F1','box-shadow':'none'});
			});

			//bordersTooltip --------------------------------------
			$('body').on('mouseover','.panel-box .border-palette',function(){
				$('.border-palette').css({'height':'100%','border':'1px solid #B9BEC3','box-shadow':'0 1px 10px rgba(0,0,0,.2)'});
			});
			$('body').on('mouseout','.panel-box .border-palette',function(){
				$('.border-palette').css({'height':'50px','border':'1px solid #F1F1F1','box-shadow':'none'});
			});
			//-----------------------------------------------------
		}
	}
};
tooltipEvents.events(true);
console.info('header/tooltipEvents');