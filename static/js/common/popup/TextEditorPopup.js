var TextEditorPopup = function(){
	this.width = 750;
	this.content = _.template(document.getElementById('text-editor-popup-template').innerHTML,{});
	this.autorun = function(){
		$('.text-editor').html(function(){return db.select([glob.CURRENT_ID],'props',['content'])});
	};
};
TextEditorPopup.prototype = new Popup();
var textEditorPopup = new TextEditorPopup();