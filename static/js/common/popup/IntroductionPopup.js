var IntroductionPopup = function(){
	this.width = 500;
	this.height = 360;
	this.content = _.template(document.getElementById('introduction-popup-template').innerHTML,{});
};
IntroductionPopup.prototype = new Popup();
var introductionPopup = new IntroductionPopup();