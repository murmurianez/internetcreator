var AddImagePopup = function(){
	this.width = 410;
	this.height = 450;
	this.content = _.template(document.getElementById('add-image-popup-template').innerHTML,{});
};
AddImagePopup.prototype = new Popup();
var addImagePopup = new AddImagePopup();