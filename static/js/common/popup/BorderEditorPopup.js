var BorderEditorPopup = function(){
	this.width = 465;
	this.height = 258;
	this.content = _.template(document.getElementById('border-editor-popup-template').innerHTML,{});
};
//		var BORDERS = $('#top-values-panel .div-tag .borders');
//		BORDERS.find('input').change(function(){
//			if(BORDERS.find('.top').prop('checked')){BORDERS.css('border-top-color','#B9BEC3');}else{BORDERS.css('border-top-color','#EEEEEE');}
//			if(BORDERS.find('.right').prop('checked')){BORDERS.css('border-right-color','#B9BEC3');}else{BORDERS.css('border-right-color','#EEEEEE');}
//			if(BORDERS.find('.bottom').prop('checked')){BORDERS.css('border-bottom-color','#B9BEC3');}else{BORDERS.css('border-bottom-color','#EEEEEE');}
//			if(BORDERS.find('.left').prop('checked')){BORDERS.css('border-left-color','#B9BEC3');}else{BORDERS.css('border-left-color','#EEEEEE');}
//		});

BorderEditorPopup.prototype.addBorders = function(){
	$('.my-set').append(function(){
		return '<div style="float:left; width:39px; height:38px; background:' +$('.result').css('border')+ '"></div>';
	});

	$('.border-palette .borders').append(function(){
		return '<div class="background" data-value="div_css_background" style="background:' +$('.result').css('border')+ '"></div>';
	});
};

BorderEditorPopup.prototype.result = function(){
	borderEditorPopup.elem.val($('.result').css('background'));
	borderEditorPopup.elem.change();
};

BorderEditorPopup.prototype = new Popup();
var borderEditorPopup = new BorderEditorPopup();