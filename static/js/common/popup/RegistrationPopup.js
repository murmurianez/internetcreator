var RegistrationPopup = function(){
	this.width = 600;
	this.height = 300;
	this.contentStyle = 'style="padding:20px"';
	this.content = 	'<div class="login-reg-popup">' +
						'<div class="login"><h2>Вход</h2>' +
							'<form action="/application/blocks/header/model/login.php" method="POST">' +
								'<div>' +
									'<label for="reg-email">E-mail:</label><br /><input type="text" id="reg-email" name="email" /><br />' +
									'<label for="reg-pass">Пароль:</label><br /><input type="password" id="reg-pass" name="password" /><a href="javascript:" class="show-pass">#</a><br />' +
									'<input type="submit" name="submit" value="Войти" />' +
								'</div>' +
							'</form>' +
						'</div>' +
						'<div class="registration"><h2>Регистрация</h2>' +
							'<form action="/application/blocks/header/model/registration.php" method="POST">' +
								'<div>' +
									'<label for="reg-email">Ваш e-mail:</label><br /><input type="text" id="reg-email" name="email" /><br />' +
									'<label for="reg-pass">Пароль:</label><br /><input type="password" id="reg-pass" name="password" /><a href="javascript:" class="show-pass">#</a><br />' +
									'<label for="reg-confirm">Повторите пароль:</label><br /><input type="password" id="reg-confirm" name="password" /><a href="javascript:" class="show-pass">#</a><br />' +
									'<input type="submit" name="submit" value="Зарегестрироваться" />' +
								'</div>' +
							'</form>' +
						'</div>' +
					'</div>';
};
RegistrationPopup.prototype = new Popup();
var registrationPopup = new RegistrationPopup();

console.info('common/popup/registrationPopup');