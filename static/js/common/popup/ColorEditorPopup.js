var ColorEditorPopup = function(){
	this.width = 893;
	this.height = 445;
	this.buttons = 	'<a href="javascript:" class="close-button cyan-button">Закрыть</a>' +
		'<a href="javascript:" class="result-color-button cyan-button">Применить</a>';
	this.content = _.template(document.getElementById('color-editor-popup-template').innerHTML,{});
	this.gradient = {
		colorNum:0,
		addColors:function(){
			$('.gradient').append(	'<div class="color">' +
										'<input type="checkbox" />' +
										'<div class="gradient-color i' + colorEditorPopup.gradient.colorNum + '"></div>' +
										'<div class="delete-button">' +
											'<i class="icon icon-black icon-delete"></i>' +
										'</div>' +
									'</div>');
			$('.gradient-color.i' + colorEditorPopup.gradient.colorNum).css('background-color',function(){
				return $('.color-picker').css('background-color');
			});
			colorEditorPopup.gradient.colorNum = colorEditorPopup.gradient.colorNum + 1;
			colorEditorPopup.gradient.colors();
		},

		colors:function(){
			if(arguments.length === 0){
				$('.result').css('background',function(){
					var direction = $('.direction').val() + 'deg';
					var color = '';

					for(var i = 0; i <= colorEditorPopup.gradient.colorNum-1; i++){
						var thisColor = $('.gradient-color.i' + i).css('background-color');
						var k = i * (100 / colorEditorPopup.gradient.colorNum)|0;
						color = color + ', ' + thisColor + ' ' + k + '%';
					}

					colorEditorPopup.resultValue = '-webkit-linear-gradient('+ direction + color + ')';
					return '-webkit-linear-gradient('+ direction + color + ')';
				});
			}else{
				$('.result').css('background',arguments[0]);
			}
		}
	};
};

ColorEditorPopup.prototype.autorun = function(){
	var hsl = document.getElementById('hsl');
	var hslContext = hsl.getContext('2d');
	var gradient = new Image();
	gradient.src = '/static/img/system/colorpicker_gradient.png';

	gradient.onload = function(){
		hslContext.drawImage(gradient,0,0,20,150);
	};

	hsl.onmousemove = function(event){
		var mouseX, mouseY;

		if(event.offsetX){
			mouseX = event.offsetX;
			mouseY = event.offsetY;
		}else{
			if(event.layerX){
				mouseX = event.layerX;
				mouseY = event.layerY;
			}
		}

		var c = hslContext.getImageData(mouseX, mouseY, 1, 1).data;
		var o = $('.opacity').val() / 100;

		$('.current').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
	};

	hsl.onmousedown = function(event){
		var mouseX, mouseY;

		if(event.offsetX){
			mouseX = event.offsetX;
			mouseY = event.offsetY;
		}else{
			if(event.layerX){
				mouseX = event.layerX;
				mouseY = event.layerY;
			}
		}
		var c = hslContext.getImageData(mouseX, mouseY, 1, 1).data;
		var o = $('.opacity').val() / 100;

		pickerContext.fillStyle = 'rgba('+c[0]+','+c[1]+','+c[2]+','+o+')';
		pickerContext.clearRect(0,0,150,150);
		pickerContext.fillRect(0,0,150,150);
		pickerContext.drawImage(img,0,0);

		$('.color-picker').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+o+')');
	};

	var picker = document.getElementById('picker');
	var pickerContext = picker.getContext('2d');
	var img = new Image();
	img.src = '/static/img/system/colorpicker_overlay.png';

	img.onload = function(){
		pickerContext.fillStyle = 'rgb(255,0,0)';
		pickerContext.clearRect(0,0,150,150);
		pickerContext.strokeRect(0,0,150,150);
		pickerContext.drawImage(img,0,0);
	};

	picker.onmousemove = function(event){
		var mouseX, mouseY;

		if(event.offsetX) {
			mouseX = event.offsetX;
			mouseY = event.offsetY;
		}else{
			if(event.layerX) {
				mouseX = event.layerX;
				mouseY = event.layerY;
			}
		}

		var c = pickerContext.getImageData(mouseX, mouseY, 1, 1).data;
		$('.rrgb').val(c[0]);
		$('.grgb').val(c[1]);
		$('.brgb').val(c[2]);
		var o = $('.opacity').val() / 100;
		$('.current').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
	};

	picker.onmousedown = function(event){
		var mouseX, mouseY;

		if(event.offsetX){
			mouseX = event.offsetX;
			mouseY = event.offsetY;
		}else{
			if(event.layerX){
				mouseX = event.layerX;
				mouseY = event.layerY;
			}
		}

		var c = pickerContext.getImageData(mouseX, mouseY, 1, 1).data;
		$('.rrgb').val(c[0]);
		$('.grgb').val(c[1]);
		$('.brgb').val(c[2]);
		var o = $('.opacity').val() / 100;
		$('.color-picker').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
	};
};

ColorEditorPopup.prototype.addBackground = function(){
	$('.my-set').append(function(){
		return '<div style="float:left; width:40px; height:40px; background:' +$('.result').css('background')+ '"></div>';
	});

	$('.palette .backgrounds').append(function(){
		return '<div class="background" data-value="div_css_background" style="background:' +$('.result').css('background')+ '"></div>';
	});

	settings.colorPalette.push($('.result').css('background'));
	$('.color-picker-popup .color-description').focus();
};

ColorEditorPopup.prototype.result = function(){
	colorEditorPopup.elem.val($('.result').css('background'));
	colorEditorPopup.elem.change();
};

ColorEditorPopup.prototype = new Popup();
var colorEditorPopup = new ColorEditorPopup();