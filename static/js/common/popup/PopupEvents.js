var popupEvents = {
	events:function(eventsOn){
		if(eventsOn){
			$('body').on('mousedown','.header',function(event){
				popup.drag(event,$(this).parent());
			});
			$('body').on('mousedown','.close-button',function(){
				popup.hide($(this).parent());
			});


			//registrationPopup -----------------------------------
			$('body').on('mousedown','.show-pass',function(){
				$('input[type="password"]').attr("type","text");
			});
			$('body').on('mouseup','.show-pass',function(){
				$('input[type="text"]').attr("type","password");
			});
			//-----------------------------------------------------


			//colorPopup ------------------------------------------
//				glob.BODY.on('click','.result-color-button',function(){formElems.read($('.result'),$('.result').css('background'))});

				glob.BODY.on('click','.add-color',function(event){event.stopImmediatePropagation(); colorEditorPopup.gradient.addColors();});
				glob.BODY.on('change','.direction',function(){colorEditorPopup.gradient.colors();});
				glob.BODY.on('change','.opacity',function(){$('.argb').val(function(){return $('.opacity').val()/100;});});
				glob.BODY.on('change','#white-colorpicker',function(){$('.transparent').css('background-image','');});
				glob.BODY.on('change','#transparent-colorpicker',function(){$('.transparent').css('background-image','url(/static/img/system/transparent.png)');});
				glob.BODY.on('click','.delete-button',function(){$(this).parent().remove()});
//				glob.BODY.on('click','.result-button',function(){colorEditorPopup.result();});
				glob.BODY.on('click','.sets .color-set div, .sets .gradients div, .sets .textures div',function(){
					var that = $(this);
					colorEditorPopup.gradient.colors(function(){
						return that.css('background')
					});
				});
				glob.BODY.on('click','.add-background-button',function(event){event.stopImmediatePropagation(); colorEditorPopup.addBackground()});
			//-----------------------------------------------------


			//shadowPopup -----------------------------------------
				glob.BODY.on('click','.shadow .add-shadow',function(){shadowEditorPopup.shadow();});
				glob.EDITOR.on('mousedown','.light-point',function(event){event.stopPropagation(); shadowEditorPopup.dragLightPoint(event,$(this));});
			//-----------------------------------------------------


			//textEditor ------------------------------------------
				glob.BODY.on('click','.text-edit-panel .button',function(){formElems.read($('.text-editor'),$('.text-editor').html())});

				glob.BODY.on('keyup','.text-editor',function(){formElems.read($(this),$(this).html());});
				glob.BODY.on('keydown','.text-editor',function(event){event.stopPropagation();});
				glob.BODY.on('click','.wisiwig-undo',function(){document.execCommand('undo',false,null );});
				glob.BODY.on('click','.wisiwig-redo',function(){document.execCommand('redo',false,null);});
				glob.BODY.on('click','.wisiwig-strong',function(){document.execCommand('bold',false,null );});
				glob.BODY.on('click','.wisiwig-italic',function(){document.execCommand('italic',false,null);});
				glob.BODY.on('click','.wisiwig-strike',function(){document.execCommand('strikeThrough',false,null);});
				glob.BODY.on('click','.wisiwig-underline',function(){document.execCommand('underline',false,null);});
				glob.BODY.on('click','.wisiwig-sub',function(){document.execCommand('subscript',false,null);});
				glob.BODY.on('click','.wisiwig-sup',function(){document.execCommand('superscript',false,null);});
				glob.BODY.on('click','.wisiwig-a',function(){
					var link = $('#wisiwig-link').val();
					if(link === ''){
						$('#wisiwig-link').prop('placeholder','Укажите ссылку');
					}else{
						document.execCommand('createLink',false,link);
					}
				});
				glob.BODY.on('click','.wisiwig-remove-a',function(){document.execCommand('unlink',false,null);});
				glob.BODY.on('click','.wisiwig-insert-char',function(){document.execCommand('insertText',false,'$');});
				glob.BODY.on('click','.wisiwig-unordered-list',function(){document.execCommand('insertUnorderedList',false,null);});
				glob.BODY.on('click','.wisiwig-ordered-list',function(){document.execCommand('insertOrderedList',false,null);});
				glob.BODY.on('click','.wisiwig-remove-format',function(){document.execCommand('removeFormat',false,null);});
				//indent - TAB(9)
				//outdent - SHIFT+TAB
			//-----------------------------------------------------

			//addImagePopup ---------------------------------------
				glob.BODY.on('click','.thumb',function(event){
					event.stopImmediatePropagation();
					activity.create('image');
					var imgSrc = $(this).find('img').attr('src');
					db.insert(glob.CURRENT_ID,'html',{'src':[imgSrc]});
					view.draw();
					view.html();
					view.css();
					select.focus();
				});
			//-----------------------------------------------------
		}
	}
};
console.info('header/popupEvents');