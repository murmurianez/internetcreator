var AddVideoPopup = function(){
	this.width = 400;
	this.height = 250;
	this.content = _.template(document.getElementById('add-video-popup-template').innerHTML,{});
};
AddVideoPopup.prototype = new Popup();
var addVideoPopup = new AddVideoPopup();