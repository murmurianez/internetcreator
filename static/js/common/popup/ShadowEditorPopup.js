var ShadowEditorPopup = function(){
	this.width = 221;
	this.height = 190;
	this.content = _.template(document.getElementById('shadow-editor-popup-template').innerHTML,{});

	this.shadow = function(){
		var el = glob.CURRENT;
		var lightX = glob.EDITOR.find('.light-point').position().left;
		var lightY = glob.EDITOR.find('.light-point').position().top;

		var shadowX = (el.offset().left + (el.height()/2) - lightX)/10|0;
		var shadowY = (el.offset().top + (el.width()/2) - lightY)/10|0;

		var shadowBlur = 112; //Отдалённость
		var shadowSize = -50; //Высота
		var shadowOpacity = 1; //Освещение
		var shadowColor = 'rgba(50,50,50,' + shadowOpacity + ')';
		var shadows;


		if(db.select([glob.CURRENT_ID],'props',['type']) === 'header'||db.select([glob.CURRENT_ID],'props',['type']) === 'p'||db.select([glob.CURRENT_ID],'props',['type']) === 'a'){
			shadows = shadowX + "px " + shadowY + "px " + 2 + "px " + shadowColor;
			db.insert(glob.CURRENT_ID,'css',{'text-shadow':[shadows]});
		}else{
			shadows = shadowX + "px " + shadowY + "px " + shadowBlur + "px " + shadowSize + "px " + shadowColor + ', inset ' + -shadowX + "px " + -shadowY + "px " + shadowBlur + "px " + shadowSize + "px " + 'rgba(50,50,50,' + shadowOpacity/2 + ')';
			db.insert(glob.CURRENT_ID,'css',{'box-shadow':[shadows]});
		}
		view.draw();
		view.css();
	};

	this.addLightPoint = function(){
		glob.PAGE.append('<div class="light-point"><div class="light-circle"></div></div>');
	};

	this.removeLightPoint = function(elem){
		elem.remove();
	};

	this.dragLightPoint = function(event,elem){
		var shiftX = event.pageX - elem.position().left;
		var shiftY = event.pageY - elem.position().top;

		glob.BODY.on('mousemove',glob.EDITOR,function(event){
			window.requestAnimationFrame(function(){
				var left = (event.pageX - shiftX)|0;
				var top = (event.pageY - shiftY)|0;
				elem.css({'left':left,'top':top});
				shadowEditorPopup.shadow();
			});
		});

		$(document).mouseup(function(){
			$(document).off('mousemove');
		});
	};
};
ShadowEditorPopup.prototype = new Popup();
var shadowEditorPopup = new ShadowEditorPopup();