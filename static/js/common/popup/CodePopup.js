var CodePopup = function(){
	this.resizable = true;
	this.buttons = 	'<a href="javascript:" class="close-button cyan-button">Закрыть</a>';
	this.content = 	'<textarea class="code" style="border:none; width:' + (this.realWidth() - 40) + 'px; height:' + (this.realHeight() - 107) + 'px"></textarea>';
	this.autorun = function(){
	}
};
CodePopup.prototype = new Popup();
var codePopup = new CodePopup();