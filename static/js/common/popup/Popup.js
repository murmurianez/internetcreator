var Popup = function(){
	this.dragable = true;
	this.resizable = false;
	this.modalWindow = false;
	this.singleton = true;
	this.autorun = function(){};
	this.elem = '';
	this.width = 600;
	this.height = 500;
	this.realWidth = function(){return this.width};
	this.realHeight = function(){return this.height + 70};
	this.top = function(){
		var y = ($(window).height() - this.realHeight())/2 - 100;
		if(y < 0){y = 0}
		return y;
	};
	this.left = function(){return ($(window).width() - this.realWidth())/2;};
	this.buttons = 	'<a href="javascript:" class="close-button cyan-button">Закрыть</a>';
	this.contentStyle = '';
	this.content = '';
	this.hide = function(elem){
		elem.parent().remove();
		popupEvents.events(false);
	};
};



Popup.prototype.show = function(elem){
	popupEvents.events(true);
	this.elem = elem;
	$('body').append('' +
		'<div class="popup" style="' +
			'left:' + this.left() + 'px;' +
			'top:' + this.top() + 'px;' +
			'width:' + this.realWidth() + 'px;' +
			'height:' + this.realHeight() + 'px;' +
			'">' +
			'<div class="header">' + this.buttons + '</div>' +
			'<div class="content" ' + this.contentStyle + '>' + this.content + '</div>' +
		'</div>');
	this.autorun();
};



Popup.prototype.drag = function(event,elem){
	//Костыль для текстового редактора - чтоб при перемещении окна случайно не выделялся текст
	$('.text-editor').addClass('unselectable');
	//----------------------------------------------------------------------------------------
	var shiftX = event.pageX - elem.position().left;
	var shiftY = event.pageY - elem.position().top;

	var left, top = null; //Garbage Collector blocking
	$(document).on('mousemove','body',function(event){
		window.requestAnimationFrame(
			function(){
				left = (event.pageX - shiftX)|0;
				top = (event.pageY - shiftY)|0;
				elem.css({'left':left,'top':top});
			}
		);
	});

	$(document).mouseup(function(){
		//Костыль для текстового редактора - чтоб при перемещении окна случайно не выделялся текст
		$('.text-editor').removeClass('unselectable');
		//----------------------------------------------------------------------------------------
		$(document).off('mousemove');
	});
};



var popup = new Popup();
console.info('common/popup/main');