var glob = {
	BODY:$('body'),
	SNAP_TO_GRID:true,
	EDITOR:$('#editor'),
	SYSTEM:$('#system'),
	PAGE:$('#page'),
	CODE_PANEL:$('#code-panel'),
	TOP_VALUES_PANEL:$('#top-values-panel'),
	PARENT:$('#page'),
	PARENT_ID:$('#page').data('id'),
	CURRENT:'',
	CURRENT_ID:''
};
console.info('siteeditpro/glob');