var view = {//display
    draw:function(){
        //Не оптимальный код - вместо цикла нужно доработать db.select, чтобы можно было выбирать все эти данные за один запрос
            var str = {};
            var css = {};
            var k = '';
            for(var prop in db.select([glob.CURRENT_ID],'css')){
                //Костыль - сделать нормальное хранение сложных CSS в db + нормальный парсинг
                if(prop === 'rotate'){
                    str['transform'] = 'rotate(' + db.select([glob.CURRENT_ID],'css',[prop]) + db.select([glob.CURRENT_ID],'css',[prop],1) + ')';
                }else{
                    k = db.select([glob.CURRENT_ID],'css',[prop]) +  db.select([glob.CURRENT_ID],'css',[prop],1);
                    str[prop] = k;
                }
                _.extend(css,str);
            }
			glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]').css(css);
			var content = db.select([glob.CURRENT_ID],'props',['content']);
			glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]').html(content);
			var html = db.select([glob.CURRENT_ID],'html');
			glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]').attr(html);
    },

    css:function(){
        var elementType = db.select([glob.CURRENT_ID],'props',['tag']);

        var result = '';
        for(var prop in db.select([glob.CURRENT_ID],'css')){
            if(db.select([glob.CURRENT_ID],'css',[prop]) !== undefined||''){
                result = result + '<li>' + prop + ':' + db.select([glob.CURRENT_ID],'css',[prop]) + db.select([glob.CURRENT_ID],'css',[prop],1) + '</li>';
            }
        }

        //Если элемент существует, мы его перезаписываем, а не добавляем
        if(glob.CODE_PANEL.find('.css [data-id="' + glob.CURRENT_ID + '"]').length > 0){
			glob.CODE_PANEL.find('.css [data-id="' + glob.CURRENT_ID + '"]').html('#' + db.select([glob.CURRENT_ID],'html',['id']) + '{<ul>'+ result +'</ul>}');
        }else{
			glob.CODE_PANEL.find('.css').append('<div data-id="' + glob.CURRENT_ID + '">#' + db.select([glob.CURRENT_ID],'html',['id']) + '{<ul>'+ result +'</ul>}</div>');
        }
    },

	html:function(){
		var deep = '';
		var parent = db.select([glob.CURRENT_ID],'props',['parent']);
		var elementType = db.select([glob.CURRENT_ID],'props',['tag']);
		var content = db.select([glob.CURRENT_ID],'props',['content']);

		var result = '';
		for(var prop in db.select([glob.CURRENT_ID],'html')){
			if(db.select([glob.CURRENT_ID],'html',[prop]) !== undefined||''){
				result = result + ' ' + prop + '="' + db.select([glob.CURRENT_ID],'html',[prop]) + '"';
			}
		}

//		var element = '&#60;' + elementType + result + '&#62;' + content + '&#60;/' + elementType + '&#62;';
		var element = '&#60;' + elementType + result + '&#62;' + _.escape(content) + '&#60;/' + elementType + '&#62;';

		//Если родитель элемента есть в наборе
		if(glob.CODE_PANEL.find('.html [data-id="' + parent + '"]').length){
			if(parent === undefined||''){
				glob.CODE_PANEL.find('.html').append('<div data-id="' + glob.CURRENT_ID + '">' + element + '</div>');
			}else{
				glob.CODE_PANEL.find('.html [data-id="' + parent + '"]').after('<div data-id="' + glob.CURRENT_ID + '">' + element + '</div>');
			}
			//Свигаем
			glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').css('margin-left',20*deep+'px');
		}else{
			//Обновляем существующий
			if(glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').length > 0){
				glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').html(element);
			}else{
				glob.CODE_PANEL.find('.html').append('<div data-id="' + glob.CURRENT_ID + '">' + element + '</div>');
			}
		}
	},

    code:function(){
        var head = 	'<!DOCTYPE html>\n' +
					'<head>\n' +
					'<meta charset="utf-8">\n' +
					'<link href="http://fonts.googleapis.com/css?family=Poiret+One|Philosopher|Yeseva+One|Tenor+Sans|Oranienbaum|Lobster|Cuprum|Open+Sans:800|Roboto:400,100|Roboto+Condensed:300,400|Forum|Prosto+One|Marmelad&subset=latin,cyrillic" rel="stylesheet" type="text/css">\n' +
					'<style>\n';
        var body = '</style>\n</head>\n<body style="background-color:#EEEEEE">\n<div style="position:relative; width:960px; height:1280px; margin:20px auto; background-color:#FFFFFF">\n';
        var end = '</div>\n</body>\n</html>';

        var prop = '';
        var css = '';
        var html = '';

        for(var j = 0, k = db.dbdata.length-1; j <= k; j++){

			for(var props in db.dbdata[j].css){
				var unit = '';
				if(db.dbdata[j].css[props][1] !== undefined||''){
					unit = db.dbdata[j].css[props][1]
				}
				prop = prop + '\n\t' + props + ':' + db.dbdata[j].css[props][0] + unit + ';';
			}

			css = css + '#' + db.dbdata[j].html['id'] + '{' + prop + '\n}\n';
			prop = '';

			for(var props in db.dbdata[j].html){
				prop = prop + ' ' + props + '="' + db.dbdata[j].html[props] + '"';
			}

			html = html + '\t<' + db.dbdata[j].props['tag'] + prop + '>' + db.dbdata[j].props['content'] + '</' + db.dbdata[j].props['tag'] + '>\n';
			prop = '';

		}

        var code = head + css + body + html + end;
        $('.popup .code').text(code);
    }
};
console.info('siteeditpro/view');