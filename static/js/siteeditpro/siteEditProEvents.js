var siteEditProEvents = {
	events:function(){
		//Событие изменения размера окна браузера -------------
			var codePanelHeight = $(window).height()-171;
			glob.CODE_PANEL.css('height',codePanelHeight);

			$(window).resize(function(){
				codePanelHeight = $(window).height()-171;
				glob.CODE_PANEL.css('height',codePanelHeight);
				if(glob.CURRENT){
					var rotate = db.select([glob.CURRENT_ID],'css',['transform']);
					var x = glob.CURRENT.position().left - 12;
					glob.EDITOR.find('.focus').css({left:x});
				}
			});
		//-----------------------------------------------------


		//Горячие клавиши -------------------------------------
			$(document).on('keydown',function(event){editor.hotKeys(event);});
		//-----------------------------------------------------


		//Выделение элемента ----------------------------------
			glob.CODE_PANEL.find('.css, .html').on('click','div',function(){select.unfocus(); glob.CURRENT_ID = $(this).data('id'); select.focus();});
			glob.BODY.on('mousedown','#editor',function(){select.unfocus();});
		//-----------------------------------------------------





		//Изменение параметров элемента из верхней панели -----
			glob.BODY.on('change','#snap-to-grid',function(){
				if($(this).prop('checked')){
					glob.SNAP_TO_GRID = true;
				}else{
					glob.SNAP_TO_GRID = false;
				}
			});

			glob.TOP_VALUES_PANEL.on('change','[data-value="header_css_font-size_px"]',function(){
				var that = $(this);
				if($('#header-font-line').prop('checked')){
					$('[data-value="header_css_line-height_px"]').val(function(){return that.val()}).change();
				}
			});

//			glob.TOP_VALUES_PANEL.on('change','[data-value="header_css_line-height_px"]',function(){
//				var that = $(this);
//				if($('#header-font-line').prop('checked')){
//					$('[data-value="header_css_font-size_px"]').val(that.val()).change();
//				}
//			});

			glob.TOP_VALUES_PANEL.on('change','[data-value="p_css_font-size_px"]',function(){
				var that = $(this);
				if($('#p-font-line').prop('checked')){
					$('[data-value="p_css_line-height_px"]').val(function(){return Math.ceil(that.val()*1.6)}).change();
				}
			});

			glob.BODY.on('change','input[type="number"], input[type="text"], textarea, select, input[type="color"]',function(){formElems.read($(this),$(this).val());});
			glob.BODY.on('keyup','input[type="number"], input[type="text"], textarea, select, input[type="color"]',function(){formElems.read($(this),$(this).val());});
			glob.BODY.on('click','.background',function(){formElems.read($(this),$(this).css('background'))});
		//-----------------------------------------------------


		//Редактирование элемента -----------------------------
//			glob.EDITOR.on('dblclick','.focus',function(){activity.edit();});
//			glob.EDITOR.on('dblclick','.darkness',function(){activity.stopEdit();});
		//-----------------------------------------------------


		//Перетаскивание рабочей области ----------------------
			glob.EDITOR.mousedown(function(event){$(this).css('cursor','url(/static/img/sprites/drag.png),move'); editor.clickX = event.pageX; editor.clickY = event.pageY; editor.pull($(this));});
		//-----------------------------------------------------


		//Перетаскивание элемента -----------------------------
			glob.PAGE.on('mousedown','.dragable',function(event){event.stopPropagation(); event.preventDefault(); activity.clickX = event.pageX; activity.clickY = event.pageY; select.unfocus(); glob.CURRENT = $(this); glob.CURRENT_ID = $(this).data('id'); select.focus(); drag.call(this,event);});
			glob.PAGE.on('mousedown','.focus',function(event){event.stopPropagation(); activity.clickX = event.pageX; activity.clickY = event.pageY; activity.drag(event);});
			function drag(event){event.stopPropagation(); activity.clickX = event.pageX; activity.clickY = event.pageY; activity.drag(event);}
		//-----------------------------------------------------


		//Изменение размера элемента --------------------------
			glob.PAGE.on('mousedown','.top-left',function(event){event.stopPropagation(); activity.resize('top-left');});
			glob.PAGE.on('mousedown','.top-right',function(event){event.stopPropagation(); activity.resize('top-right');});
			glob.PAGE.on('mousedown','.bottom-left',function(event){event.stopPropagation(); activity.resize('bottom-left');});
			glob.PAGE.on('mousedown','.bottom-right',function(event){event.stopPropagation(); activity.resize('bottom-right');});
		//-----------------------------------------------------


		//Поворот элемента ------------------------------------
			glob.PAGE.on('mousedown','.rotate',function(event){event.stopPropagation(); activity.rotate();});
		//-----------------------------------------------------


		//Изменение размера боковой панели --------------------
			glob.CODE_PANEL.find('.resize-code-panel').mousedown(function(){editor.codePanelResize();});
		//-----------------------------------------------------


		//Направляющие ----------------------------------------
			glob.EDITOR.find('.top-ruler').mousedown(function(event){guides.addHorizontal(event,$(this));});
			glob.EDITOR.find('.left-ruler').mousedown(function(event){guides.addVertical(event,$(this));});
			glob.EDITOR.find('.ruler-button').mousedown(function(event){guides.addHorizontal(event,$(this)); guides.addVertical(event,$(this));});
			glob.EDITOR.on('mousedown','.horizontal-line',function(event){guides.moveHorizontal(event,$(this));});
			glob.EDITOR.on('mousedown','.vertical-line',function(event){guides.moveVertical(event,$(this));});
		//-----------------------------------------------------

		//Переименование элемента -----------------------------
			$('#element-name').change(function(){activity.rename($(this).val());});
			$('#element-name').keyup(function(){activity.rename($(this).val());});
		//-----------------------------------------------------


		//Зум -------------------------------------------------
			$('#editor-scale').change(function(){editor.scale($(this).val());});
		//-----------------------------------------------------


//		//Вывод координат курсора внизу страницы --------------
//			var xPos = document.getElementById('cursorPageX');
//			var yPos = document.getElementById('cursorPageY');
//			glob.EDITOR.mousemove(function(event){xPos.innerText = event.pageX; yPos.innerText = event.pageY});
//		//-----------------------------------------------------



		glob.BODY.on('click','.close-button',function(){codePopup.hide($(this));});

		glob.BODY.on('change','.files',function(){activity.addImage(event);});
		glob.BODY.on('click','.video-apply',function(){activity.addVideo($('.popup .video').val());});

		glob.BODY.on('click','.border-palette-show',function(){borderEditorPopup.show($(this));});



		$('#code-panel').find('.result-button').click(function(){codePopup.show($(this)); view.code();});
		$('.shadow-editor-show').click(function(){shadowEditorPopup.show($(this));});
		glob.BODY.on('click','.color-picker-show',function(){colorEditorPopup.show($(this));});


		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//Почему отсюда вызывается нормально один раз, а из popupEvents - 2??????????????????????????????????????
		glob.BODY.on('click','.add-light-point',function(){shadowEditorPopup.addLightPoint();});
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		introductionPopup.show();
//		setInterval(function(){ls.load()},3000);
	}
};
siteEditProEvents.events();

console.info('siteeditpro/siteEditProEvents');