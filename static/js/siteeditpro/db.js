var db = {
    dbdata:[],

    create:function(){
        var Elems = function(){
            //resize: true, false, proportional, vertical, horizontal
            this.props = [];
            this.css = [];
            this.html = [];
        };
        db.dbdata[db.dbdata.length] = new Elems();
		return db.dbdata[db.dbdata.length-1];
    },

	copy:function(currentId){
		var copy = library.copy(db.dbdata[currentId-1]);
		copy = _.extend(db.create(),copy);
		copy.props.id[0] = db.dbdata.length;
		return copy;
	},

    insert:function(elems,path,props){
        _.extend(db.dbdata[elems-1][path],props);
    },

    del:function(elems,type,props){
        if(props === undefined||''){
            db.dbdata[elems] = '';
        }else{
            db.dbdata[elems].type.props = '';
        }
    },

//Варианты использования:
    select:function(elems,path,props,info){ //[elems],dbMemPath,[props],1

        if(info === undefined||''){info = 0}

        for(var j = 0, k = elems.length-1; j <= k; j++){

			if (props === undefined || '') {	//возвращает объект - вывод кода
				var result = {};
				for (var prop in db.dbdata[elems - 1][path]) {
					var variable = db.dbdata[elems - 1][path][prop][info];
					if (variable !== undefined || '') {
						result[prop] = variable;
					}
				}
			} else {	//возвращает строку - получение одного заданного свойства
				var result = '';
				for (var i = 0, n = props.length - 1; i <= n; i++) {
					if (this.dbdata[elems - 1][path][props[i]] !== undefined || '') {
						var variable = db.dbdata[elems - 1][path][props[i]][info];
					}
					if (variable !== undefined || '') {
						result = result + variable;
					}
				}
			}
        }
        return result;
    }
};

var ls = {
	load:function(){

		//db.dbdata = JSON.parse(localStorage.getItem('db'));
		console.log(JSON.stringify(db.dbdata,"",4));
	},

	localSave:function(){
		localStorage.setItem('db',JSON.stringify(db.dbdata));
	},

	serverSave:function(){
		$.ajax({
			type: "POST",
			url: "ajax.php",
			dataType: 'JSON',

			data: {
				db: JSON.stringify(db.dbdata)
			},

			beforesend: $('#top .save-button').text('Идёт сохранение...'),

			success: function(data, code){
				if (code==200){
					$('.content').html(data); // запрос успешно прошел
				}else{
					$('.content').html(code); // возникла ошибка, возвращаем код ошибки
				}

				$('#top .save-button').text('Сохранено 3 минуты назад');
			},

			error:  function(xhr, str){
				$('#top .save-button').text('Проверьте подключение к Интернет');
			},

			complete: function(){}
		});
	}
};
console.info('siteeditpro/db');