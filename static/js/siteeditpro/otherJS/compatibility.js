var compatibility = {
	css:function(css){
		switch(property){
			case 'rotate':compatibility.rotate(value); break;
			case 'border-radius':compatibility.borderRadius(value); break;
			case 'linear-gradient':compatibility.linearGradient(value); break;
		}
	},

	rgba:function(value){
		//filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#886287a7', endColorstr='#886287a7');
	},

	rotate:function(value){
		//filter: progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', M11=0.7071067811865476, M12=-0.7071067811865475, M21=0.7071067811865475, M22=0.7071067811865476); /* IE6,IE7 */
		//-ms-filter: "progid:DXImageTransform.Microsoft.Matrix(SizingMethod='auto expand', M11=0.7071067811865476, M12=-0.7071067811865475, M21=0.7071067811865475, M22=0.7071067811865476)"; /* IE8 */

		//filter: progid:DXImageTransform.Microsoft.Matrix(
		//    M11 = COS_THETA,
		//    M12 = -SIN_THETA,
		//    M21 = SIN_THETA,
		//    M22 = COS_THETA,
		//    sizingMethod = 'auto expand'
		//);
		//-ms-filter: "progid:DXImageTransform.Microsoft.Matrix(
		//M11 = COS_THETA,
		//    M12 = -SIN_THETA,
		//    M21 = SIN_THETA,
		//    M22 = COS_THETA,
		//    SizingMethod = 'auto expand'
		//)";
		//Where COS_THETA and SIN_THETA are the cosine and sine values of the angle (i.e. 0.70710678 for 45°).

		return '-webkit-' + value;
	},

	linearGradient:function(value){

		//    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#886287a7', endColorstr='#886287a7');

		return '-webkit-' + value;
	},

	borderRadius:function(value){

	}
};
console.info('siteeditpro/other/compatibility');