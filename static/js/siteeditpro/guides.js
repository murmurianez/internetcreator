var guides = {
	addHorizontal:function(event,that){
		glob.EDITOR.css('cursor','row-resize');
		event.stopPropagation();
		that.mouseout(_.once(function(){
			var i = glob.EDITOR.find('.horizontal-line').length + 1;
			glob.EDITOR.append('<div class="horizontal-line numhorizontal' + i + '"><div></div></div>');
			glob.EDITOR.mousemove(function(event){

				if(event.shiftKey){
					glob.SNAP_TO_GRID = false;
				}else{
					glob.SNAP_TO_GRID = true;
				}

				if(glob.SNAP_TO_GRID){
					// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
					var y = (event.pageY/10|0)*10 - 141;
				}else{
					var y = event.pageY - 141;
				}

				glob.EDITOR.find('.horizontal-line.numhorizontal' + i).css('top', y);
				glob.EDITOR.find('.top-ruler').off('mousedown, mouseout');
				glob.EDITOR.mouseup(function(){
					glob.EDITOR.css('cursor','url("/static/img/sprites/move.png"),move');
					glob.EDITOR.off('mousemove');
				})
			})
		}))
	},

	addVertical:function(event,that){
		glob.EDITOR.css('cursor','col-resize');
		event.stopPropagation();
		that.mouseout(_.once(function(){
			var j = glob.EDITOR.find('.vertical-line').length + 1;
			glob.EDITOR.append('<div class="vertical-line numvertical' + j +'"><div></div></div>');
			glob.EDITOR.mousemove(function(event){

				if(event.shiftKey){
					glob.SNAP_TO_GRID = false;
				}else{
					glob.SNAP_TO_GRID = true;
				}

				if(glob.SNAP_TO_GRID){
					// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
					var x = (event.pageX/10|0)*10 - 46;
				}else{
					var x = event.pageX - 46;
				}

				glob.EDITOR.find('.vertical-line.numvertical' + j).css('left',x);
				glob.EDITOR.find('.left-ruler').off('mousedown, mouseout');
				glob.EDITOR.mouseup(function(){
					glob.EDITOR.css('cursor','url("/static/img/sprites/move.png"),move');
					glob.EDITOR.off('mousemove');
				})
			});
		}));
	},

	moveHorizontal:function(event,that){
		glob.EDITOR.css('cursor','row-resize');
		event.stopPropagation();
		glob.EDITOR.mousemove(function(event){

			if(event.shiftKey){
				glob.SNAP_TO_GRID = false;
			}else{
				glob.SNAP_TO_GRID = true;
			}

			if(glob.SNAP_TO_GRID){
				// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
				var y = (event.pageY/10|0)*10 - 141;
			}else{
				var y = event.pageY - 141;
			}

			that.css('top',y);

			glob.BODY.mouseup(function(){
				glob.EDITOR.off('mousemove');
				if(that.position().top < 16){
					that.remove();
				}
			});
		});
	},

	moveVertical:function(event,that){
		glob.EDITOR.css('cursor','col-resize');
		event.stopPropagation();
		glob.EDITOR.mousemove(function(event){

			if(event.shiftKey){
				glob.SNAP_TO_GRID = false;
			}else{
				glob.SNAP_TO_GRID = true;
			}

			if(glob.SNAP_TO_GRID){
				// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
				var x = (event.pageX/10|0)*10 - 46;
			}else{
				var x = event.pageX - 46;
			}

			that.css('left',x);

			glob.BODY.mouseup(function(){
				glob.EDITOR.off('mousemove');
				if(that.position().left < 16){
					that.remove();
				}
			});
		});
	}
};
console.info('siteeditpro/guides');