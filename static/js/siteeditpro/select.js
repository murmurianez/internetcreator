var select = {

//    hover:function(element){ //$(this)
//		if(document.getElementsByClassName('overlay').length === 0){
//			$('body').append('<div class="overlay"></div>');
//			$('.overlay').data('id',glob.CURRENT_ID);
//		}
//
//		var width = element.outerWidth();
//		var height = element.outerHeight();
//		var rotate = element.css('transform');

//		var top = element.position().top-10;
//		var left = element.position().left-10;
//		$('.overlay').css({'width':width,'height':height,'top':top,'left':left,'-webkit-transform':rotate});
//    },
//
//    unhover:function(){
//        $('.overlay').remove();
//    },
	focusNum:0,
    focus:function(){
		var focusElems = (function(){
			var count;
			return count++;
		})();

        glob.CURRENT = glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]');

		var r = '<div class="drag rotate"></div>';
		var s = '<div class="drag bottom-right"></div>'; //'<div class="drag top-left"></div><div class="drag top-right"></div><div class="drag bottom-left"></div>'

		glob.PAGE.append('<div class="focus">' + s + r + '</div>');

        var width = glob.CURRENT.outerWidth();
        var height = glob.CURRENT.outerHeight();
        var rotate = db.select([glob.CURRENT_ID],'css',['rotate']);//glob.CURRENT.css('rotate');

		var top = (db.select([glob.CURRENT_ID],'css',['top'])-10)|0 + 'px';//(glob.CURRENT.position().top-10)|0;
		var left = (db.select([glob.CURRENT_ID],'css',['left'])-10)|0 + 'px'; //(glob.CURRENT.position().left-10)|0;
        glob.EDITOR.find('.focus').css({'width':width,'height':height,'top':top,'left':left,'transform':'rotate('+rotate+'deg)'});

        formElems.write();

        glob.CODE_PANEL.find('.css [data-id="' + glob.CURRENT_ID + '"]').addClass('hover');
        glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').addClass('hover');

        glob.TOP_VALUES_PANEL.find('.element-name h3').text(function(){
            return db.select([glob.CURRENT_ID],'props',['type']) + glob.CURRENT_ID;
        });
    },

    unfocus:function(){
        glob.CURRENT = '';
        glob.CURRENT_ID = '';

        glob.TOP_VALUES_PANEL.find('.element-name h3').text('body');
		glob.TOP_VALUES_PANEL.find('.properties').hide();
		glob.TOP_VALUES_PANEL.find('.body-tag').show();

		select.focusNum = 0;
        glob.PAGE.find('.drag, .focus').remove();
        glob.CODE_PANEL.find('.css *').removeClass('hover');
		glob.CODE_PANEL.find('.html *').removeClass('hover');
    },

	renewFocus:function(){
		glob.PAGE.find('.drag, .focus').remove();
		glob.CURRENT = glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]');


		var r = '<div class="drag rotate"></div>';
		var s = '<div class="drag bottom-right"></div>'; //'<div class="drag top-left"></div><div class="drag top-right"></div><div class="drag bottom-left"></div>'
		glob.PAGE.append('<div class="focus">' + s + r + '</div>');

		var width = glob.CURRENT.outerWidth();
		var height = glob.CURRENT.outerHeight();
		var rotate = db.select([glob.CURRENT_ID],'css',['rotate']);//glob.CURRENT.css('rotate');
		var top = (db.select([glob.CURRENT_ID],'css',['top'])-10)|0 + 'px';//(glob.CURRENT.position().top-10)|0;
		var left = (db.select([glob.CURRENT_ID],'css',['left'])-10)|0 + 'px'; //(glob.CURRENT.position().left-10)|0;

		glob.EDITOR.find('.focus').css({'width':width,'height':height,'top':top,'left':left, 'transform':'rotate('+rotate+'deg)'});
	}
};
console.info('siteeditpro/select');