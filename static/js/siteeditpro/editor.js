var editor = {
	clickX:'',
	clickY:'',
	zoom:1,

	pull:function(element){
		var shiftX = editor.clickX - glob.PAGE.position().left - (glob.PAGE.width()/2|0) * (editor.zoom - 1);
		var shiftY = editor.clickY - glob.PAGE.position().top - (glob.PAGE.height()/2|0) * (editor.zoom - 1);

		glob.EDITOR.mousemove(function(event){
			window.requestAnimationFrame(function(){
				var x = (event.pageX - shiftX)|0;
				var y = (event.pageY - shiftY)|0;
				glob.PAGE.css({left:x,top:y});

				glob.EDITOR.find('.top-ruler-left').css('right',(glob.EDITOR.width() - glob.PAGE.offset().left)/editor.zoom - glob.CODE_PANEL.width() - 8);
				glob.EDITOR.find('.top-ruler-right').css('left',(glob.PAGE.offset().left - 41)/editor.zoom);
				glob.EDITOR.find('.left-ruler-top').css('bottom',(glob.EDITOR.height() - glob.PAGE.offset().top)*editor.zoom - 39);
				glob.EDITOR.find('.left-ruler-bottom').css('top',(glob.PAGE.offset().top - 140)/editor.zoom);
			});
		});

		$(document).mouseup(function(){
			glob.EDITOR.off('mousemove');
			element.css('cursor','url(/static/img/sprites/move.png),move');
		});
	},

	fullscreen:function(){
		if(document.webkitIsFullScreen){
			document.webkitCancelFullScreen()
		}else{
			document.getElementById('body').webkitRequestFullScreen()
		}
	},

	codePanelResize:function(){
		glob.BODY.mousemove(function(event){
			window.requestAnimationFrame(function(){
				var x = $(window).width() - event.pageX + 36;
				glob.CODE_PANEL.css('width',x);
				glob.EDITOR.css('width',$(window).width() - x - 40);
				glob.BODY.mouseup(function(){
					glob.BODY.off('mousemove');
				});
			});
		});
	},

	showPanel:function(panelName){
		glob.CODE_PANEL.find('.css, .html, .js').hide();
		switch(panelName){
			case 'css':glob.CODE_PANEL.find('.content .css').show(); break;
			case 'html':glob.CODE_PANEL.find('.content .html').show(); break;
			case 'js':glob.CODE_PANEL.find('.content .js').show(); break;
		}
	},

	hotKeys:function(event){
		switch(event.keyCode){
			case 13: activity.edit(); break; //ENTER Смена родительского объекта на текущий элемент
//			case 16: break; //SHIFT Выделение нескольких объектов
			//case 27: $('.darkness').remove(); glob.PARENT_ID = ''; break; //ESC Прекращение редактирования объекта
			case 37: event.preventDefault(); activity.drag('',{horizontal:-1}); break; //LEFT
			case 38: event.preventDefault(); activity.drag('',{vertical:-1}); break; //UP
			case 39: event.preventDefault(); activity.drag('',{horizontal:1}); break; //RIGHT
			case 40: event.preventDefault(); activity.drag('',{vertical:1}); break; //DOWN
			case 187: event.preventDefault(); editor.scale('5'); $(document).keyup(function(){editor.scale('0')}); break; //+
			case 189: event.preventDefault(); editor.scale('-3'); $(document).keyup(function(){editor.scale('0')}); break; //-
			case 46: event.preventDefault(); activity.del(); break; //DELETE
			case 122: event.preventDefault(); editor.fullscreen(); break; //F11
			case 186: event.preventDefault(); if(event.ctrlKey){$('#editor .horizontal-line, #editor .vertical-line, .light-point').toggle();} break; //CTRL + ;
			/* Shift+MouseClick выделить несколько объектов, Ctrl+MouseDrag dublicate, Ctrl+C Ctrl+Insert copy, Ctrl+V Shift+Insert paste, Ctrl+Z undo, Ctrl+Y redo */
		}
		//SHIFT + Click - Выделить несколько элементов
		//CTRL + Zoom - preventDefault, editor scale
		//+ (43) - мax zoom - На время удержания клавиши
		//- (45) - min zoom - На время удержания клавиши
	},

	scale:function(value){
		var scale = function(){
			var result;
			switch (value){
				case '-3': result = 0.25; glob.PAGE.css('background','#FFFFFF'); break;
				case '-2': result = 0.5; glob.PAGE.css('background','#FFFFFF'); break;
				case '-1': result = 0.7; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '0': result = 1; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '1': result = 1.5; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '2': result = 3; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '3': result = 5; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '4': result = 7; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '5': result = 10; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
			}
			return result;
		};

		if(scale() !== 0){
			$('#page, #editor .focus, #editor .overlay, #editor .horizontal-line, #editor .vertical-line').css('-webkit-transform','scale(' + scale() + ')');
			glob.EDITOR.find('.top-ruler').css('-webkit-transform','scale(' + scale() + ',' + 1 + ')');
			glob.EDITOR.find('.left-ruler').css('-webkit-transform','scale(' + 1 + ',' + scale() + ')');
			editor.zoom = scale();
		}
	}
};
console.info('siteeditpro/editor');