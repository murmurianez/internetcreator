$(function(){
    $('#page .leftBtn').click(function(){slideLeft()});
    $('#page .rightBtn').click(function(){slideRight()});

    var slideRight = function(){
        $('#page .slide').first().before(function(){
            return $('.slide').last();
        });
    };

    var slideLeft = function(){
        $('#page .slide').last().after(function(){
            return $('#page .slide').first();
        });
    };

    setInterval(function(){
        slideLeft();
    },7000);
});
console.info('siteeditpro/append');