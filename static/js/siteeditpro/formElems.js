var formElems = {
    read:function(elem,value){ //$(this), $(this).val();
        var data = elem.data('value');
        var elInfo = data.split('_');

        var props = {};
        props[elInfo[2]] = [value,elInfo[3]];
        db.insert(glob.CURRENT_ID,elInfo[1],props);

        view.draw();
        view.css();
        view.html();
		select.renewFocus();
    },

    write:function(){   //<input type="number" data-value="p_css_width_px" />
        glob.TOP_VALUES_PANEL.find('.properties').hide();

        var elemType = db.select([glob.CURRENT_ID],'props',['type']);
        glob.TOP_VALUES_PANEL.find('.' + elemType + '-tag').show();

        glob.TOP_VALUES_PANEL.find('.' + elemType + '-tag input, .' + elemType + '-tag textarea').each(function(){
            var that = this;
            var value = (function(){
                var data = $(that).data('value');

				//Костыль - undefined - не вдаваясь в подробности - без него валятся ошибки и сбивается копирование по горячим клавишам
				if(data !== undefined){
					var elInfo = data.split('_');
					return db.select([glob.CURRENT_ID],elInfo[1],[elInfo[2]]);
				}

            })();
            $(this).val(value);
        });
		select.renewFocus();
    }
};
console.info('siteeditpro/formElems');