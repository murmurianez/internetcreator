'use strict';
module.exports = function(grunt){
	grunt.initConfig({
		concat:{
			main:{
				src:[
					'static/js/siteeditpro/glob.js',
					'static/js/common/library.js',

					'static/js/common/popup/Popup.js',
					'static/js/common/popup/RegistrationPopup.js',
					'static/js/common/popup/LoginPopup.js',
					'static/js/common/popup/PopupEvents.js',
					'static/js/header/headerEvents.js',
					'static/js/common/tooltip/tooltip.js',
					'static/js/common/tooltip/tooltipEvents.js'
				],
				dest:'static/js/main.min.js'
			},

			siteeditpro:{
				src:[
					'static/js/common/popup/IntroductionPopup.js',
					'static/js/common/popup/ColorEditorPopup.js',
					'static/js/common/popup/BorderEditorPopup.js',
					'static/js/common/popup/AddImagePopup.js',
					'static/js/common/popup/ShadowEditorPopup.js',
					'static/js/common/popup/TextEditorPopup.js',

					'static/js/siteeditpro/activity.js',
					'static/js/siteeditpro/select.js',
					'static/js/siteeditpro/editor.js',
					'static/js/siteeditpro/db.js',
					'static/js/siteeditpro/view.js',
					'static/js/siteeditpro/formElems.js',
					'static/js/siteeditpro/elements.js',
					'static/js/siteeditpro/append.js',
					'static/js/siteeditpro/guides.js',
					'static/js/siteeditpro/siteEditProEvents.js'
				],
				dest:'static/js/siteeditpro.min.js'
			},

			userpage:{
				src:[
					'static/js/userpage/main.js',
					'static/js/common/popup/changeAvatarPopup.js'
				],
				dest:'static/js/userpage.min.js'
			},

			manifest:{
				src:['<%= grunt.template.today("m-d-yyyy") %>'],
				dest:'build/appcache.mf'
			}
		},

		'closure-compiler': {
			frontend: {
				closurePath: '/src/to/closure-compiler',
				js: 'static/src/frontend.js',
				jsOutputFile: 'static/js/frontend.min.js',
				maxBuffer: 500,
				options: {
					compilation_level: 'ADVANCED_OPTIMIZATIONS',
					language_in: 'ECMASCRIPT5_STRICT',
					externs: '<%= process.env.CLOSURE_PATH %>/contrib/externs/jquery-1.7.js',
				}
			}
		},


		uglify:{
			main:{
				files:{
					'static/js/main.min.js':'<%= concat.main.dest %>',
					'static/js/siteeditpro.min.js':'<%= concat.siteeditpro.dest %>',
					'static/js/userpage.min.js':'<%= concat.userpage.dest %>'
				}
			}
		},

		htmlmin:{},

		imagemin:{
			dist:{
				options:{
					optimizationLevel:7
				},
				files: [{
					expand: true,
					cwd: 'static/img',
					src: '{,*/}*.{png,jpg,jpeg}',
					dest: 'build/img'
				}]
			}
		},

		jshint:{}
	});

	grunt.loadNpmTasks('grunt-contrib-handlebars');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-imagemin');

	grunt.registerTask('default',['concat']);
	grunt.registerTask('build',['concat','uglify','imagemin']);
};