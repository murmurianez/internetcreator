var glob = {
	BODY:$('body'),
	SNAP_TO_GRID:true,
	EDITOR:$('#editor'),
	SYSTEM:$('#system'),
	PAGE:$('#page'),
	CODE_PANEL:$('#code-panel'),
	TOP_VALUES_PANEL:$('#top-values-panel'),
	PARENT:$('#page'),
	PARENT_ID:$('#page').data('id'),
	CURRENT:'',
	CURRENT_ID:''
};
console.info('siteeditpro/glob');
var library = {

//Копирование объекта с разрывом связей (каждый живёт своей жизнью)
	clone: function(obj){
		if(!obj || typeof obj !== 'object'){
			return obj;
		}
		var c = (typeof obj.pop === 'function') ? [] : {};
		var p, v;
		for(p in obj){
			if(obj.hasOwnProperty(p)){
				v = obj[p];
				if(v && typeof v === 'object'){
					c[p] = library.clone(v);
				}else{
					c[p] = v;
				}
			}
		}
		return c;
	}











//	function wheel(event){
//	var delta = 0;
//	if (!event) event = window.event; // Событие IE.
//	// Установим кроссбраузерную delta
//	if (event.wheelDelta) {
//		// IE, Opera, safari, chrome - кратность дельта равна 120
//		delta = event.wheelDelta/120;
//	} else if (event.detail) {
//		// Mozilla, кратность дельта равна 3
//		delta = -event.detail/3;
//	}
//	if (delta) {
//		// Отменим текущее событие - событие поумолчанию (скролинг окна).
//		if (event.preventDefault) {
//			event.preventDefault();
//		}
//		event.returnValue = false; // для IE
//
//		// если дельта больше 0, то колесо крутят вверх, иначе вниз
//		var dir = delta > 0 ? 'Up' : 'Down',
//			}
//	}















//    //Наследование объекта
//    inherit: function(p){
//        if(p == null){throw TypeError()}
//        if(Object.create){return Object.create(p)}
//
//        var t = typeof p;
//        if(t !== "object" && t !== "function"){throw TypeError()}
//        function f(){}
//        f.prototype = p;
//        return new f();
//    },
////
//    //Копирование свойств из второго объекта в первый с заменой
//    extend: function(o,p){
//        for(var prop in p){
//            o[prop] = p[prop];
//        }
//        return o;
//    },
//
//
//    //Копирование свойств из одного объекта в другой без замены
//    merge: function(o,p){
//        for(prop in p){
//            if(o.hasOwnProperty[prop]){continue};
//            o[prop] = p[prop];
//        }
//    },
//
//    //Удаление из первого объекта свойств отсутствующих во втором
//    restrict: function(o,p){
//        for(prop in o){
//            if(!(prop in p)){
//                delete o[prop];
//            }
//        }
//        return o;
//    },
//
//    //Удаление из первого объекта свойств присутствующих во втором
//    subtract: function(o,p){
//        for(prop in p){
//            delete o[prop];
//        }
//    },
//
//    //Возвращает новый объект со свойствами обоих объектов. Если свойства совпадают - предпочтение отдаётся свойствам второго объекта
//    union: function(o,p){
//        return library.extend(library.extend({},o),p);
//    },
//
//    //Возвращает объект со свойсвами присутствующими в обоих объектах. Если свойства совпадают - предпочтение отдаётся свойствам второго объекта
//    intersection: function(o,p){
//        return library.restrict(library.extend({},o),p);
//    },
//
//    //Возвращение массива с именами свойств объекта
//    keys: function(o){
//        if(typeof o !== "object"){throw TypeError()}
//        if(Object.keys){return Object.keys(o)}
//        var result = [];
//        for(var prop in o){
//            if(o.hasOwnProperty(prop)){
//                result.push(prop);
//            }
//        };
//        return result;
//    }
};
console.info('common/library');
var Popup = function(){
	this.dragable = true;
	this.resizable = false;
	this.modalWindow = false;
	this.singleton = true;

	this.autorun = function(){};
	this.elem = '';
	this.width = 600;
	this.height = 500;
	this.realWidth = function(){return this.width};
	this.realHeight = function(){return this.height + 70};
	this.top = function(){return ($(window).height() - this.realHeight())/2 - 100;};
	this.left = function(){return ($(window).width() - this.realWidth())/2;};

	this.buttons = 	'<a href="javascript:" class="close-button cyan-button">Закрыть</a>';
	this.contentStyle = '';
	this.content = '';

	this.show = function(elem){
		popupEvents.events(true);
		this.elem = elem;
		$('body').append('' +
			'<div class="popup" style="' +
			'left:' + this.left() + 'px;' +
			'top:' + this.top() + 'px;' +
			'width:' + this.realWidth() + 'px;' +
			'height:' + this.realHeight() + 'px;' +
			'">' +
			'<div class="header">' + this.buttons + '</div>' +
			'<div class="content" ' + this.contentStyle + '>' + this.content + '</div>' +
			'</div>');
		this.autorun();
	};

	this.drag = function(event,elem){
		//Костыль для текстового редактора - чтоб при перемещении окна случайно не выделялся текст
		$('.text-editor').addClass('unselectable');
		//----------------------------------------------------------------------------------------
		var shiftX = event.pageX - elem.position().left;
		var shiftY = event.pageY - elem.position().top;

		var left, top = null; //Garbage Collector blocking
		$(document).on('mousemove','body',function(event){
			window.requestAnimationFrame(
				function(){
					left = (event.pageX - shiftX)|0;
					top = (event.pageY - shiftY)|0;
					elem.css({'left':left,'top':top});
				}
			);
		});

		$(document).mouseup(function(){
			//Костыль для текстового редактора - чтоб при перемещении окна случайно не выделялся текст
			$('.text-editor').removeClass('unselectable');
			//----------------------------------------------------------------------------------------
			$(document).off('mousemove');
		});
	};

	this.hide = function(elem){
		elem.parent().remove();
		popupEvents.events(false);
	};
};
var popup = new Popup();


console.info('common/popup/main');
var RegistrationPopup = function(){
	this.width = 600;
	this.height = 300;
	this.contentStyle = 'style="padding:20px"';
	this.content = 	'<div class="login-reg-popup">' +
						'<div class="login"><h2>Вход</h2>' +
							'<form action="/application/blocks/header/model/login.php" method="POST">' +
								'<div>' +
									'<label for="reg-email">E-mail:</label><br /><input type="text" id="reg-email" name="email" /><br />' +
									'<label for="reg-pass">Пароль:</label><br /><input type="password" id="reg-pass" name="password" /><a href="javascript:" class="show-pass">#</a><br />' +
									'<input type="submit" name="submit" value="Войти" />' +
								'</div>' +
							'</form>' +
						'</div>' +
						'<div class="registration"><h2>Регистрация</h2>' +
							'<form action="/application/blocks/header/model/registration.php" method="POST">' +
								'<div>' +
									'<label for="reg-email">Ваш e-mail:</label><br /><input type="text" id="reg-email" name="email" /><br />' +
									'<label for="reg-pass">Пароль:</label><br /><input type="password" id="reg-pass" name="password" /><a href="javascript:" class="show-pass">#</a><br />' +
									'<label for="reg-confirm">Повторите пароль:</label><br /><input type="password" id="reg-confirm" name="password" /><a href="javascript:" class="show-pass">#</a><br />' +
									'<input type="submit" name="submit" value="Зарегестрироваться" />' +
								'</div>' +
							'</form>' +
						'</div>' +
					'</div>';
};
RegistrationPopup.prototype = new Popup();
var registrationPopup = new RegistrationPopup();

console.info('common/popup/registrationPopup');
var LoginPopup = function(){
	this.width = 360;
	this.height = 200;
	this.contentStyle = 'style="padding:20px"';
	this.content = 	'<form action="/application/blocks/header/model/login.php" method="POST">' +
						'<input type="text" name="email" placeholder="e-mail" />' +
						'<input type="password" name="password" placeholder="password" />' +
						'<input type="submit" name="submit" value="submit" />' +
					'</form>';
};
LoginPopup.prototype = new Popup();
var loginPopup = new LoginPopup();

console.info('common/popup/loginPopup');
var popupEvents = {
	events:function(eventsOn){
		if(eventsOn){
			$('body').on('mousedown','.header',function(event){popup.drag(event,$(this).parent());});
			$('body').on('mousedown','.close-button',function(){popup.hide($(this).parent());});


			//registrationPopup -----------------------------------
			$('body').on('mousedown','.show-pass',function(){$('input[type="password"]').attr("type","text");});
			$('body').on('mouseup','.show-pass',function(){$('input[type="text"]').attr("type","password");});
			//-----------------------------------------------------


			//colorPopup ------------------------------------------
//				glob.BODY.on('click','.result-color-button',function(){formElems.read($('.result'),$('.result').css('background'))});

				glob.BODY.on('click','.add-color',function(event){event.stopImmediatePropagation(); colorEditorPopup.gradient.addColors();});
				glob.BODY.on('change','.direction',function(){colorEditorPopup.gradient.colors();});
				glob.BODY.on('change','.opacity',function(){$('.argb').val(function(){return $('.opacity').val()/100;});});
				glob.BODY.on('change','#white-colorpicker',function(){$('.transparent').css('background-image','');});
				glob.BODY.on('change','#transparent-colorpicker',function(){$('.transparent').css('background-image','url(/static/img/system/transparent.png)');});
				glob.BODY.on('click','.delete-button',function(){$(this).parent().remove()});
//				glob.BODY.on('click','.result-button',function(){colorEditorPopup.result();});
				glob.BODY.on('click','.sets .color-set div, .sets .gradients div, .sets .textures div',function(){
					var that = $(this);
					colorEditorPopup.gradient.colors(function(){
						return that.css('background')
					});
				});
				glob.BODY.on('click','.add-background-button',function(event){event.stopImmediatePropagation(); colorEditorPopup.addBackground()});
			//-----------------------------------------------------


			//shadowPopup -----------------------------------------
				glob.BODY.on('click','.shadow .add-shadow',function(){shadowEditorPopup.shadow();});
				glob.EDITOR.on('mousedown','.light-point',function(event){event.stopPropagation(); shadowEditorPopup.dragLightPoint(event,$(this));});
			//-----------------------------------------------------


			//textEditor ------------------------------------------
				glob.BODY.on('click','.text-edit-panel .button',function(){formElems.read($('.text-editor'),$('.text-editor').html())});

				glob.BODY.on('keyup','.text-editor',function(){formElems.read($(this),$(this).html());});
				glob.BODY.on('keydown','.text-editor',function(event){event.stopPropagation();});
				glob.BODY.on('click','.wisiwig-undo',function(){document.execCommand('undo',false,null );});
				glob.BODY.on('click','.wisiwig-redo',function(){document.execCommand('redo',false,null);});
				glob.BODY.on('click','.wisiwig-strong',function(){document.execCommand('bold',false,null );});
				glob.BODY.on('click','.wisiwig-italic',function(){document.execCommand('italic',false,null);});
				glob.BODY.on('click','.wisiwig-strike',function(){document.execCommand('strikeThrough',false,null);});
				glob.BODY.on('click','.wisiwig-underline',function(){document.execCommand('underline',false,null);});
				glob.BODY.on('click','.wisiwig-sub',function(){document.execCommand('subscript',false,null);});
				glob.BODY.on('click','.wisiwig-sup',function(){document.execCommand('superscript',false,null);});
				glob.BODY.on('click','.wisiwig-a',function(){
					var link = $('#wisiwig-link').val();
					if(link === ''){
						$('#wisiwig-link').prop('placeholder','Укажите ссылку');
					}else{
						document.execCommand('createLink',false,link);
					}
				});
				glob.BODY.on('click','.wisiwig-remove-a',function(){document.execCommand('unlink',false,null);});
				glob.BODY.on('click','.wisiwig-insert-char',function(){document.execCommand('insertText',false,'$');});
				glob.BODY.on('click','.wisiwig-unordered-list',function(){document.execCommand('insertUnorderedList',false,null);});
				glob.BODY.on('click','.wisiwig-ordered-list',function(){document.execCommand('insertOrderedList',false,null);});
				glob.BODY.on('click','.wisiwig-remove-format',function(){document.execCommand('removeFormat',false,null);});
				//indent - TAB(9)
				//outdent - SHIFT+TAB
			//-----------------------------------------------------

			//addImagePopup ---------------------------------------
				glob.BODY.on('click','.thumb',function(event){
					event.stopImmediatePropagation();
					activity.create('image');
					var imgSrc = $(this).find('img').attr('src');
					db.insert(glob.CURRENT_ID,'html',{'src':[imgSrc]});
					view.draw();
					view.html();
					view.css();
					select.focus();
				});
			//-----------------------------------------------------
		}
	}
};
console.info('header/popupEvents');
var headerEvents = {
	events:function(){
		$('#top .login-registration a').click(function(){registrationPopup.show();});
	}
};
headerEvents.events();
console.info('header/headerEvents');