var mainUserpage = {
	autoload:function(){
		$('#userpage .avatar img, #userpage .avatar-change-button').hover(function(){
			$('#userpage .avatar-change-button').toggle()
		});

		$('#userpage .avatar-change-button').click(function(){
			changeAvatarPopup.show($(this));
		});

		$('.user-highlight').change(function(){
			switch($(this).val()){
				case 'default': $('#userpage .avatar img').css('border','3px solid #F1F1F1'); break;
				case 'red': $('#userpage .avatar img').css('border','3px solid rgb(180,8,9)'); break;
				case 'green': $('#userpage .avatar img').css('border','3px solid rgb(165,225,1)'); break;
				case 'yellow': $('#userpage .avatar img').css('border','3px solid rgb(250,193,56)'); break;
				case 'gray': $('#userpage .avatar img').css('border','3px solid rgb(85,85,85)'); break;
			}
		});
	}
};
mainUserpage.autoload();
var ChangeAvatarPopup = function(){
	this.width = 400;
	this.height = 200;
	this.contentStyle = 'style="padding:20px"';
	this.content = 	'<h3>Выберите фото</h3>' +
					'<form method="POST" name="email_form_with_php" action="?fileupload" enctype="multipart/form-data">' +
						'<input type="file" name="uploaded_file" id="uploaded_file" >' +
						'<input type="submit" value="Submit" name="submit">' +
					'</form>';
};
ChangeAvatarPopup.prototype = new Popup();
var changeAvatarPopup = new ChangeAvatarPopup();