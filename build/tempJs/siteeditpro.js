var IntroductionPopup = function(){
	this.width = 500;
	this.height = 360;
	this.content = _.template(document.getElementById('introduction-popup-template'));
};
IntroductionPopup.prototype = new Popup();
var introductionPopup = new IntroductionPopup();
var ColorEditorPopup = function(){
	this.width = 893;
	this.height = 445;
	this.buttons = 	'<a href="javascript:" class="close-button cyan-button">Закрыть</a>' +
//		'<a href="javascript:" class="result-color-button cyan-button">Применить</a>' +
		'';
	this.content = 	'<div class="color-picker-popup">' +

						'<div class="picker">' +

							'<canvas id="hsl" width="18" height="148"></canvas>' +

							'<div class="alpha">' +
								'<input type="range" class="opacity" min="0" max="100" value="100" />' +
							'</div>' +

							'<div class="transparent">' +
								'<canvas id="picker" width="148" height="148"></canvas>' +
							'</div>' +

							'<div class="change-colors transparent">' +
								'<div class="color-picker"></div>' +
								'<div class="current"></div>' +
							'</div>' +

							'<div class="add-color" title="Добавить цвет в градиент">' +
								'<div class="line"></div>' +
								'<div class="add-button">' +
									'<i class="icon icon-black icon-a"></i>' +
									'<div class="triangle"></div>' +
								'</div>' +
							'</div>' +

							'<div class="transparent"><div class="gradient"></div></div>' +

							'<div class="transparent"><div class="result"></div></div>' +

							'<div class="add-background-button" title="Добавить в свой набор">' +
								'<div class="line"></div>' +
								'<div class="add-button">' +
									'<i class="icon icon-black icon-a"></i>' +
									'<div class="triangle"></div>' +
								'</div>' +
							'</div>' +
						'</div>' +




						'<div class="my-set transparent"></div>'+
						'<textarea class="color-description" placeholder="Описание применения цвета . . ."></textarea>'+




						'<div class="color-value">' +
							'<div>R: <input type="number" class="rrgb" min="0" max="255" /></div>' +
							'<div>G: <input type="number" class="grgb" min="0" max="255" /></div>' +
							'<div>B: <input type="number" class="brgb" min="0" max="255" /></div>' +
							'<div>A: <input type="number" class="argb" min="0" max="1" value="1" /></div>' +
							'<div class="input">Direction: <input type="number" class="direction" min="0" max="359" value="0" /></div>' +
						'</div>' +




						'<div class="sets">' +

							'<div class="gradients transparent">' +
								'<div style="background:-webkit-linear-gradient(top, rgba(169,3,41,1) 0%,rgba(143,2,34,1) 44%,rgba(109,0,25,1) 100%);"></div>' +
								'<div style="background:-webkit-linear-gradient(top, rgba(213,206,166,1) 0%,rgba(201,193,144,1) 40%,rgba(183,173,112,1) 100%);"></div>' +
								'<div style="background:-webkit-linear-gradient(top, rgba(98,125,77,1) 0%,rgba(31,59,8,1) 100%);"></div>' +
								'<div style="background:-webkit-linear-gradient(top, rgba(30,87,153,1) 0%,rgba(41,137,216,1) 50%,rgba(32,124,202,1) 51%,rgba(125,185,232,1) 100%);"></div>' +
								'<div style="background:-webkit-linear-gradient(top, rgba(210,255,82,1) 0%,rgba(145,232,66,1) 100%);"></div>' +
								'<div style="background:-webkit-linear-gradient(top, rgba(30,87,153,1) 0%,rgba(89,148,202,1) 62%,rgba(95,154,207,0.7) 68%,rgba(125,185,232,0) 100%);"></div>' +
								'<div style="background:-webkit-linear-gradient(top, rgba(216,224,222,1) 0%,rgba(174,191,188,1) 22%,rgba(153,175,171,1) 33%,rgba(142,166,162,1) 50%,rgba(130,157,152,1) 67%,rgba(78,92,90,1) 82%,rgba(14,14,14,1) 100%);"></div>' +
								'<div style="background:-webkit-gradient(linear, left top, left bottom, from(#a59179), to(#91785b))"></div>' +
							'</div>' +

							'<div class="textures transparent">' +
								'<div style="background:url(/static/img/textures/brown.png)"></div>' +
								'<div style="background:url(/static/img/textures/black.jpg)"></div>' +
								'<div style="background:url(/static/img/textures/gray.png)"></div>' +
								'<div style="background:url(/static/img/textures/crissXcross.png)"></div>' +
								'<div style="background:url(/static/img/textures/dark_mosaic.png)"></div>' +
								'<div style="background:url(/static/img/textures/tactile_noise.png)"></div>' +
								'<div style="background:url(/static/img/textures/batthern.png)"></div>' +
								'<div style="background:url(/static/img/textures/bo_play_pattern.png)"></div>' +
								'<div style="background:url(/static/img/textures/brushed_alu_dark.png)"></div>' +
								'<div style="background:url(/static/img/textures/cardboard.png)"></div>' +
								'<div style="background:url(/static/img/textures/dark_wall.png)"></div>' +
								'<div style="background:url(/static/img/textures/denim.png)"></div>' +
								'<div style="background:url(/static/img/textures/diagonal-noise.png)"></div>' +
								'<div style="background:url(/static/img/textures/greyfloral.png)"></div>' +
								'<div style="background:url(/static/img/textures/grunge_wall.png)"></div>' +
								'<div style="background:url(/static/img/textures/inflicted.png)"></div>' +
								'<div style="background:url(/static/img/textures/retina_wood.png)"></div>' +
								'<div style="background:url(/static/img/textures/tileable_wood_texture.png)"></div>' +
								'<div style="background:url(/static/img/textures/vichy.png)"></div>' +
								'<div style="background:url(/static/img/textures/wall4.png)"></div>' +
								'<div style="background:url(/static/img/textures/white_carbon.png)"></div>' +
								'<div style="background:url(/static/img/textures/wood_pattern.png)"></div>' +
								'<div style="background:url(/static/img/textures/worn_dots.png)"></div>' +
							'</div>' +

							'<div class="colors-sets transparent">' +
								'<div class="color-set">' +
									'<div style="background:#FC4639"></div>' +
									'<div style="background:#FFD541"></div>' +
									'<div style="background:#22B4AA"></div>' +
									'<div style="background:#006774"></div>' +
									'<div style="background:#391A22"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#CAFF42"></div>' +
									'<div style="background:#EBF7F8"></div>' +
									'<div style="background:#D0E0EB"></div>' +
									'<div style="background:#88ABC2"></div>' +
									'<div style="background:#49708A"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#951F2B"></div>' +
									'<div style="background:#F5F4D7"></div>' +
									'<div style="background:#E0DFB1"></div>' +
									'<div style="background:#A5A36C"></div>' +
									'<div style="background:#535233"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#A8A7A7"></div>' +
									'<div style="background:#CC527A"></div>' +
									'<div style="background:#E8175D"></div>' +
									'<div style="background:#474747"></div>' +
									'<div style="background:#363636"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#E9E0D1"></div>' +
									'<div style="background:#91A398"></div>' +
									'<div style="background:#33605A"></div>' +
									'<div style="background:#070001"></div>' +
									'<div style="background:#68462B"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#5E412F"></div>' +
									'<div style="background:#FCEBB6"></div>' +
									'<div style="background:#78C0A8"></div>' +
									'<div style="background:#F07818"></div>' +
									'<div style="background:#F0A830"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#EEE6AB"></div>' +
									'<div style="background:#C5BC8E"></div>' +
									'<div style="background:#696758"></div>' +
									'<div style="background:#45484B"></div>' +
									'<div style="background:#36393B"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#D1E751"></div>' +
									'<div style="background:#FFFFFF"></div>' +
									'<div style="background:#000000"></div>' +
									'<div style="background:#4DBCE9"></div>' +
									'<div style="background:#26ADE4"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#ECD078"></div>' +
									'<div style="background:#D95B43"></div>' +
									'<div style="background:#C02942"></div>' +
									'<div style="background:#542437"></div>' +
									'<div style="background:#53777A"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#D9CEB2"></div>' +
									'<div style="background:#948C75"></div>' +
									'<div style="background:#D5DED9"></div>' +
									'<div style="background:#7A6A53"></div>' +
									'<div style="background:#99B2B7"></div>' +
								'</div>' +
								'<div class="color-set">' +
									'<div style="background:#F4F1EF"></div>' +
									'<div style="background:#FFFFFF"></div>' +
									'<div style="background:#FA4B2A"></div>' +
									'<div style="background:#666666"></div>' +
									'<div style="background:#333333"></div>' +
								'</div>' +
							'</div>' +

						'</div>' +
					'</div>' ;




	this.autorun = function(){
		var hsl = document.getElementById('hsl');
		var hslContext = hsl.getContext('2d');
		var gradient = new Image();
		gradient.src = '/static/img/system/colorpicker_gradient.png';

		gradient.onload = function(){
			hslContext.drawImage(gradient,0,0,20,150);
		};

		hsl.onmousemove = function(event){
			var mouseX, mouseY;

			if(event.offsetX){
				mouseX = event.offsetX;
				mouseY = event.offsetY;
			}else{
				if(event.layerX){
					mouseX = event.layerX;
					mouseY = event.layerY;
				}
			}

			var c = hslContext.getImageData(mouseX, mouseY, 1, 1).data;
			var o = $('.opacity').val() / 100;

			$('.current').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
		};

		hsl.onmousedown = function(event){
			var mouseX, mouseY;

			if(event.offsetX){
				mouseX = event.offsetX;
				mouseY = event.offsetY;
			}else{
				if(event.layerX){
					mouseX = event.layerX;
					mouseY = event.layerY;
				}
			}
			var c = hslContext.getImageData(mouseX, mouseY, 1, 1).data;
			var o = $('.opacity').val() / 100;

			pickerContext.fillStyle = 'rgba('+c[0]+','+c[1]+','+c[2]+','+o+')';
			pickerContext.clearRect(0,0,150,150);
			pickerContext.fillRect(0,0,150,150);
			pickerContext.drawImage(img,0,0);

			$('.color-picker').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+o+')');
		};

		var picker = document.getElementById('picker');
		var pickerContext = picker.getContext('2d');
		var img = new Image();
		img.src = '/static/img/system/colorpicker_overlay.png';

		img.onload = function(){
			pickerContext.fillStyle = 'rgb(255,0,0)';
			pickerContext.clearRect(0,0,150,150);
			pickerContext.strokeRect(0,0,150,150);
			pickerContext.drawImage(img,0,0);
		};

		picker.onmousemove = function(event){
			var mouseX, mouseY;

			if(event.offsetX) {
				mouseX = event.offsetX;
				mouseY = event.offsetY;
			}else{
				if(event.layerX) {
					mouseX = event.layerX;
					mouseY = event.layerY;
				}
			}

			var c = pickerContext.getImageData(mouseX, mouseY, 1, 1).data;
			$('.rrgb').val(c[0]);
			$('.grgb').val(c[1]);
			$('.brgb').val(c[2]);
			var o = $('.opacity').val() / 100;
			$('.current').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
		};
		
		picker.onmousedown = function(event){
			var mouseX, mouseY;

			if(event.offsetX){
				mouseX = event.offsetX;
				mouseY = event.offsetY;
			}else{
				if(event.layerX){
					mouseX = event.layerX;
					mouseY = event.layerY;
				}
			}

			var c = pickerContext.getImageData(mouseX, mouseY, 1, 1).data;
			$('.rrgb').val(c[0]);
			$('.grgb').val(c[1]);
			$('.brgb').val(c[2]);
			var o = $('.opacity').val() / 100;
			$('.color-picker').css('background-color','rgba('+c[0]+','+c[1]+','+c[2]+','+ o +')');
		};
	};

	this.gradient = {
		colorNum:0,
		addColors:function(){
			$('.gradient').append(	'<div class="color">' +
										'<input type="checkbox" />' +
										'<div class="gradient-color i' + colorEditorPopup.gradient.colorNum + '"></div>' +
										'<div class="delete-button">' +
											'<i class="icon icon-black icon-delete"></i>' +
										'</div>' +
									'</div>');
			$('.gradient-color.i' + colorEditorPopup.gradient.colorNum).css('background-color',function(){
				return $('.color-picker').css('background-color');
			});
			colorEditorPopup.gradient.colorNum = colorEditorPopup.gradient.colorNum + 1;
			colorEditorPopup.gradient.colors();
		},

		deleteColors:function(){

		},

		colors:function(){
			if(arguments.length === 0){
				$('.result').css('background',function(){
					var direction = $('.direction').val() + 'deg';
					var color = '';

					for(var i = 0; i <= colorEditorPopup.gradient.colorNum-1; i++){
						var thisColor = $('.gradient-color.i' + i).css('background-color');
						var k = i * (100 / colorEditorPopup.gradient.colorNum)|0;
						color = color + ', ' + thisColor + ' ' + k + '%';
					}

					colorEditorPopup.resultValue = '-webkit-linear-gradient('+ direction + color + ')';
					return '-webkit-linear-gradient('+ direction + color + ')';
				});
			}else{
				$('.result').css('background',arguments[0]);
			}
		}
	};

	this.result = function(){
		colorEditorPopup.elem.val($('.result').css('background'));
		colorEditorPopup.elem.change();
	};

	this.addBackground = function(){
		$('.my-set').append(function(){
			return '<div style="float:left; width:40px; height:40px; background:' +$('.result').css('background')+ '"></div>';
		});

		$('.palette .backgrounds').append(function(){
			return '<div class="background" data-value="div_css_background" style="background:' +$('.result').css('background')+ '"></div>';
		});

		settings.colorPalette.push($('.result').css('background'));
		$('.color-picker-popup .color-description').focus();
	};
};
ColorEditorPopup.prototype = new Popup();
var colorEditorPopup = new ColorEditorPopup();
var AddImagePopup = function(){
	this.width = 410;
	this.height = 450;
	this.content = '<div class="add-image-top"><h3>Выберите изображения</h3><input type="file" class="files" multiple /></div>' +
		'<div class="add-image-bottom">' +
			'<div class="thumbs">' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/1.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/2.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/3.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/4.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/5.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/6.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/7.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/8.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/9.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/10.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/11.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/12.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/13.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/14.png" /></div>' +
				'<div class="thumb"><img src="/static/img/siteeditpro/photobank/15.png" /></div>' +
			'</div>' +
		'</div>';
};
AddImagePopup.prototype = new Popup();
var addImagePopup = new AddImagePopup();
var ShadowEditorPopup = function(){
	this.width = 221;
	this.height = 190;
	this.content = 	'<div class="shadow">' +
						'<div class="inset-shadow">' +
							'<label>outset:</label><br /><input type="text" data-value="div_css_box-shadow" class="div_css_box-shadow" /><br />' +
							'<label>text outset:</label><br /><input type="text" data-value="div_css_text-shadow" class="div_css_text-shadow" /><br /><br />' +
							'<a href="javascript:" class="cyan-button add-light-point">Добавить источник света</a>' +
						'</div>' +
					'</div>';

	this.shadow = function(){
		var el = glob.CURRENT;
		var lightX = glob.EDITOR.find('.light-point').position().left;
		var lightY = glob.EDITOR.find('.light-point').position().top;

		var shadowX = (el.offset().left + (el.height()/2) - lightX)/10|0;
		var shadowY = (el.offset().top + (el.width()/2) - lightY)/10|0;

		var shadowBlur = 112; //Отдалённость
		var shadowSize = -50; //Высота
		var shadowOpacity = 1; //Освещение
		var shadowColor = 'rgba(50,50,50,' + shadowOpacity + ')';
		var shadows;


		if(db.select([glob.CURRENT_ID],'props',['type']) === 'header'||db.select([glob.CURRENT_ID],'props',['type']) === 'p'||db.select([glob.CURRENT_ID],'props',['type']) === 'a'){
			shadows = shadowX + "px " + shadowY + "px " + 2 + "px " + shadowColor;
			db.insert(glob.CURRENT_ID,'css',{'text-shadow':[shadows]});
		}else{
			shadows = shadowX + "px " + shadowY + "px " + shadowBlur + "px " + shadowSize + "px " + shadowColor + ', inset ' + -shadowX + "px " + -shadowY + "px " + shadowBlur + "px " + shadowSize + "px " + 'rgba(50,50,50,' + shadowOpacity/2 + ')';
			db.insert(glob.CURRENT_ID,'css',{'box-shadow':[shadows]});
		}
		view.draw();
		view.css();
	};

	this.addLightPoint = function(){
		glob.PAGE.append('<div class="light-point"><div class="light-circle"></div></div>');
	};

	this.removeLightPoint = function(elem){
		elem.remove();
	};

	this.dragLightPoint = function(event,elem){
		var shiftX = event.pageX - elem.position().left;
		var shiftY = event.pageY - elem.position().top;

		glob.BODY.on('mousemove',glob.EDITOR,function(event){
			window.requestAnimationFrame(function(){
				var left = (event.pageX - shiftX)|0;
				var top = (event.pageY - shiftY)|0;
				elem.css({'left':left,'top':top});
				shadowEditorPopup.shadow();
			});
		});

		$(document).mouseup(function(){
			$(document).off('mousemove');
		});
	};
};
ShadowEditorPopup.prototype = new Popup();
var shadowEditorPopup = new ShadowEditorPopup();
//strip_tags($text,'<strong><b><i><s><u><sub><sup><a><ul><ol><li>');
var TextEditorPopup = function(){
	this.width = 650;
	this.content = 	'<div class="text-edit-panel">' +
						'<a href="javascript:" class="button wisiwig-undo" title="undo"><i class="icon icon-black icon-undo"></i></a>' +
						'<a href="javascript:" class="button wisiwig-redo" title="redo"><i class="icon icon-black icon-redo"></i></a>' +
						'<a href="javascript:" class="button wisiwig-strong" title="strong"><i class="icon icon-black icon-text-strong"></i></a>' +
						'<a href="javascript:" class="button wisiwig-italic" title="italic"><i class="icon icon-black icon-text-italic"></i></a>' +
						'<a href="javascript:" class="button wisiwig-strike" title="strike"><i class="icon icon-black icon-text-strike"></i></a>' +
						'<a href="javascript:" class="button wisiwig-underline" title="underline"><i class="icon icon-black icon-text-underline"></i></a>' +
						'<a href="javascript:" class="button wisiwig-sub" title="sub"><i class="icon icon-black icon-text-sub"></i></a>' +
						'<a href="javascript:" class="button wisiwig-sup" title="sup"><i class="icon icon-black icon-text-sup"></i></a>' +
						'<a href="javascript:" class="button wisiwig-a" title="link"><i class="icon icon-black icon-a"></i></a>' +
						'<a href="javascript:" class="button wisiwig-remove-a" title="remove link"><i class="icon icon-black icon-text-remove-link"></i></a>' +
						'<a href="javascript:" class="button wisiwig-insert-char" title="insert-char"><i class="icon icon-black icon-text-insert-char"></i></a>' +
						'<a href="javascript:" class="button wisiwig-unordered-list" title="unordered list"><i class="icon icon-black icon-text-ul"></i></a>' +
						'<a href="javascript:" class="button wisiwig-ordered-list" title="ordered list"><i class="icon icon-black icon-text-ol"></i></a>' +
						'<a href="javascript:" class="button wisiwig-remove-format" title="remove format"><i class="icon icon-black icon-text-remove-format"></i></a>' +
						'<input type="url" id="wisiwig-link" placeholder="http://" />' +
					'</div>' +
					'<div contentEditable="true" data-value="p_props_content" class="text-editor">Text</div>';
	this.autorun = function(){
		$('.text-editor').html(function(){return db.select([glob.CURRENT_ID],'props',['content'])});
	};
};
TextEditorPopup.prototype = new Popup();
var textEditorPopup = new TextEditorPopup();
var activity = {
//action
    clickX:'',
    clickY:'',
    x:0,
    y:0,
    create:function(elemType){

        db.create();
        select.unfocus();
        glob.CURRENT_ID = db.dbdata.length;

        //Сдвиг элемента при создании
            var x = activity.x = activity.x + 10;
            var y = activity.y = activity.y + 10;

        switch(elemType){
			case 'body':
				db.insert(glob.CURRENT_ID,'props',{'type':'page','tag':'div','children':'true','focus':'false'});
				db.insert(glob.CURRENT_ID,'html',{'id':'page'});
				db.insert(glob.CURRENT_ID,'css',{'background-color':'#EEEEEE','background-image':'','background-position':'50% 0%','background-repeat':'no-repeat no-repeat'});
				break;
            case 'page':
                db.insert(glob.CURRENT_ID,'props',{'type':'page','tag':'div','children':'true','focus':'false'});
                db.insert(glob.CURRENT_ID,'html',{'id':'page'});
                db.insert(glob.CURRENT_ID,'css',{'position':'relative','width':'960px','height':'1280px','margin':'20px auto','background-color':'#FFFFFF'});
                break;



            case 'div':
                db.insert(glob.CURRENT_ID,"props",{"type":["div"],"tag":["div"],"children":["true"],"content":[""]});
                db.insert(glob.CURRENT_ID,"html",{"id":["div" + glob.CURRENT_ID]});
                db.insert(glob.CURRENT_ID,"css",{"position":["absolute"],"width":[400,"px"],"height":[250,"px"],"left":[x,"px"],"top":[y,"px"],"border-radius":[0,"px"],"border-width":[0,"px"],"border-style":["solid"],"border-color":["#B9BEC3"],"background-color":["rgba(59,109,150,0.1)"],"rotate":[0,"deg"]});
                break;

			case 'h2':
				db.insert(glob.CURRENT_ID,'props',{'type':['header'],'tag':['h2'],'content':['Длинный заголовок']});
				db.insert(glob.CURRENT_ID,'html',{'id':['header' + glob.CURRENT_ID]});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px'],'color':['#0B598A'],'margin':[0],'font-size':[26,'px'],'line-height':[26,'px'],'font-weight':['normal'],'word-wrap':['break-word'],'white-space':['pre-wrap'],'overflow':['hidden'],'font-family':['Cuprum']});
				break;
			case 'p':
				db.insert(glob.CURRENT_ID,'props',{'type':['p'],'tag':['p'],'content':['Те, кому когда-либо приходилось делать в квартире ремонт, наверное, обращали внимание на старые газеты, наклеенные под обоями. Как правило, пока все статьи не перечитаешь, ничего другого делать не можешь. Интересно же — обрывки текста, чья-то жизнь... Так же и с рыбой. Пока заказчик не прочтет всё, он не успокоится. Бывали случаи, когда дизайн принимался именно из-за рыбного текста, который, разумеется, никакого отношения к работе не имел.']});
				db.insert(glob.CURRENT_ID,'html',{'id':['p' + glob.CURRENT_ID]});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[360,'px'],'height':[149,'px'],'left':[x,'px'],'top':[y,'px'],'margin':[0],'font-size':[13,'px'],'line-height':[21,'px'],'color':['#333333'],'word-wrap':['break-word'],'white-space':['pre-wrap'],'overflow':['hidden'],'font-family':['Cuprum']});
				break;
			case 'a':
				db.insert(glob.CURRENT_ID,'props',{'type':['a'],'tag':['a'],'content':['Ссылка'],'resize':['false']});
				db.insert(glob.CURRENT_ID,'html',{'id':['a' + glob.CURRENT_ID],'href':['javascript:']});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px'],'color':['#0B598A'],'font-family':['Cuprum']});
				break;
			case 'button':
				db.insert(glob.CURRENT_ID,'props',{'type':['button'],'tag':['a'],'href':['javascript:'],'content':['Кнопка']});
				db.insert(glob.CURRENT_ID,'html',{'id':['button' + glob.CURRENT_ID],'href':['javascript:']});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'display':['block'],'width':[78,'px'],'height':[28,'px'],'line-height':[28,'px'],'text-align':['center'],'text-decoration':['none'],'left':[x,'px'],'top':[y,'px'],'background-color':['#0B598A'],'color':['#FFFFFF'],'border':['1px solid #B9BEC3'],'font-family':['Cuprum']});
				break;

            case 'textarea':
                db.insert(glob.CURRENT_ID,'props',{'type':['textarea'],'tag':['textarea']});
                db.insert(glob.CURRENT_ID,'html',{'id':['textarea' + glob.CURRENT_ID],'placeholder':['Lorem ipsum dolor']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[358,'px'],'height':[208,'px'],'left':[x,'px'],'top':[y,'px'],'padding':[10,'px'],'border':['1px solid #B9BEC3'],'font-size':[14,'px'],'font-family':['Cuprum']});
                break;
            case 'input':
                db.insert(glob.CURRENT_ID,'props',{'type':['input'],'tag':['input']});
                db.insert(glob.CURRENT_ID,'html',{'id':['input' + glob.CURRENT_ID],'placeholder':['Lorem ipsum dolor'],'type':['text']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[116,'px'],'height':[22,'px'],'left':[x,'px'],'top':[y,'px'],'padding-top':[2,'px'],'padding-right':[10,'px'],'padding-bottom':[2,'px'],'padding-left':[10,'px'],'font-size':[14,'px'],'font-family':['Cuprum']});
                break;
			case 'select':
				db.insert(glob.CURRENT_ID,'props',{'type':['select'],'tag':['div'],'rotate':['false']});
				db.insert(glob.CURRENT_ID,'props',{'content':[elements.select(glob.CURRENT_ID)]});
				db.insert(glob.CURRENT_ID,'html',{'id':['select' + glob.CURRENT_ID]});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px'],'font-size':[14,'px'],'font-family':['Cuprum']});
				break;
            case 'checkbox':
                db.insert(glob.CURRENT_ID,'props',{'type':['checkbox'],'tag':['div'],'text':['Checkbox']});
                db.insert(glob.CURRENT_ID,'props',{'content':[elements.checkbox(glob.CURRENT_ID)]});
                db.insert(glob.CURRENT_ID,'html',{'id':['checkbox' + glob.CURRENT_ID],'type':['checkbox']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px']});
                break;
            case 'radio':
                db.insert(glob.CURRENT_ID,'props',{'type':['radio'],'tag':['div'],'text':['Radio']});
                db.insert(glob.CURRENT_ID,'props',{'content':[elements.radio(glob.CURRENT_ID)]});
                db.insert(glob.CURRENT_ID,'html',{'id':['radio' + glob.CURRENT_ID],'type':['radio']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'left':[x,'px'],'top':[y,'px']});
                break;

/*
 				http://www.youtube.com/watch?v=1ot4pEYuz10
				http://www.youtube.com/watch?feature=player_detailpage&v=1ot4pEYuz10
				http://www.youtube.com/watch?feature=player_detailpage&v=1ot4pEYuz10#t=58s
				<iframe width="640" height="360" src="http://www.youtube.com/embed/1ot4pEYuz10?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>

				http://vk.com/video5202009_137190802
				<iframe src="http://vk.com/video_ext.php?oid=5202009&id=137190802&hash=b6be216feddad1bc" width="607" height="360" frameborder="0"></iframe>

				https://vimeo.com/67871488
				<iframe src="http://player.vimeo.com/video/67871488" width="400" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
				https://vimeo.com/67871488#t=13
*/

			case 'video':
                db.insert(glob.CURRENT_ID,'props',{'type':['video'],'tag':['iframe']});
                db.insert(glob.CURRENT_ID,'html',{'id':['video' + glob.CURRENT_ID],'width':['640'],'height':['360'],'src':['http://www.youtube.com/watch?v=QezTaLyPTv4&feature=player_detailpage'],'frameborder':['0'],'allowfullscreen':['']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[640,'px'],'height':[360,'px'],'left':[x,'px'],'top':[y,'px']});
                break;
            case 'image':
                db.insert(glob.CURRENT_ID,'props',{'type':['image'],'tag':['img'],'resize':['proportional']});
                db.insert(glob.CURRENT_ID,'html',{'id':['image' + glob.CURRENT_ID]});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[400,'px'],'left':[x,'px'],'top':[y,'px']});
                break;
            case 'slider':
                db.insert(glob.CURRENT_ID,'props',{'type':['slider'],'tag':['div']});
                db.insert(glob.CURRENT_ID,'props',{'content':[elements.slider(glob.CURRENT_ID)]});
                db.insert(glob.CURRENT_ID,'html',{'id':['slider' + glob.CURRENT_ID],'class':['slider dragable']});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[675,'px'],'height':[300,'px'],'left':[x,'px'],'top':[y,'px']});
                break;



			case 'map':
				db.insert(glob.CURRENT_ID,'props',{'type':['map'],'tag':['div']});
				db.insert(glob.CURRENT_ID,'props',{'content':[elements.map(glob.CURRENT_ID)]});
				db.insert(glob.CURRENT_ID,'html',{'id':['map' + glob.CURRENT_ID]});
				db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[600,'px'],'height':[249,'px'],'left':[x,'px'],'top':[y,'px']});
				break;
            case 'chart':
                db.insert(glob.CURRENT_ID,'props',{'type':['chart'],'tag':['div']});
                db.insert(glob.CURRENT_ID,'props',{'content':[elements.chart(glob.CURRENT_ID)]});
                db.insert(glob.CURRENT_ID,'html',{'id':['chart' + glob.CURRENT_ID]});
                db.insert(glob.CURRENT_ID,'css',{'position':['absolute'],'width':[618,'px'],'height':[416,'px'],'left':[x,'px'],'top':[y,'px']});
                break;
        }
		db.insert(glob.CURRENT_ID,"props",{"id":[glob.CURRENT_ID]});
        db.insert(glob.CURRENT_ID,"props",{"parent":[glob.PARENT_ID]});

        var result = '';
        for(var prop in db.select([glob.CURRENT_ID],'html')){
            if(db.select([glob.CURRENT_ID],'html',[prop]) !== undefined||''){
                result = result + ' ' + prop + '="' + db.select([glob.CURRENT_ID],'html',[prop]) + '"';
            }
        }

        var tag = db.select([glob.CURRENT_ID],'props',['tag']);
        var elem =  '<' + tag + ' data-id="' + glob.CURRENT_ID + '"' + result + ' class="dragable">' + db.select([glob.CURRENT_ID],'props',['content']) + '</' + tag + '>';
        glob.PARENT.append(elem);

        view.draw();
        view.css();
        view.html();
		select.focus();

		$('#element-name').attr('placeholder',function(){return db.select([glob.CURRENT_ID],'html',['id'])}).focus();

		ls.load();
    },

	addImage:function(event){
		var files = event.target.files; // FileList object

		// Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {

			// Only process image files.
			if (!f.type.match('image.*')) {
				continue;
			}

			var reader = new FileReader();

			reader.onload = (function(theFile) {
				return function(event) {
					activity.create('image');
					db.insert(glob.CURRENT_ID,'html',{'src':[event.target.result]});
					view.draw();
				};
			})(f);

			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
		addImagePopup.hide();
	},

	addVideo:function(link){
		activity.create('video');
		db.insert(glob.CURRENT_ID,'props',{'content':[link]});
		addVideoPopup.hide();
		view.draw();
	},

	rename:function(name){
		db.insert(glob.CURRENT_ID,'html',{'id':[name]});
		view.css();
		view.html();
	},

    edit:function(){
        if($('.darkness').length == 0){
            glob.EDITOR.append('<div class="darkness"></div>');
            glob.PARENT_ID = glob.CURRENT_ID;
			select.unfocus();
        }
    },

	stopEdit:function(){
		$('.darkness').remove();
		glob.PARENT_ID = '';
	},

	copy:function(){
		//Костыль: сделать нормальное условие
		if(!glob.CURRENT_ID){return false;}

		db.copy(glob.CURRENT_ID);
		glob.CURRENT_ID = db.dbdata.length;

		var type = db.select([glob.CURRENT_ID],'props',['type']);
		db.insert(glob.CURRENT_ID,'html',{'id':[type + glob.CURRENT_ID]});

		var result = '';
		for(var prop in db.select([glob.CURRENT_ID],'html')){
			if(db.select([glob.CURRENT_ID],'html',[prop]) !== undefined||''){
				result = result + ' ' + prop + '="' + db.select([glob.CURRENT_ID],'html',[prop]) + '"';
			}
		}

		var tag = db.select([glob.CURRENT_ID],'props',['tag']);
		var elem =  '<' + tag + ' data-id="' + glob.CURRENT_ID + '"' + result + ' class="dragable">' + db.select([glob.CURRENT_ID],'props',['content']) + '</' + tag + '>';

		glob.PARENT.append(elem);
		view.draw();
		view.css();
		view.html();
		select.focus();
		$('#element-name').attr('placeholder',function(){return db.select([glob.CURRENT_ID],'html',['id'])}).focus();
	},

    del:function(){
        if(glob.CURRENT){
            glob.CURRENT.remove();
            db.del(glob.CURRENT_ID,'');
            glob.CODE_PANEL.find('.css [data-id="' + glob.CURRENT_ID + '"]').remove();
			glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').remove();
			select.unfocus();
        }
    },

    drag:function(event,drag){
        if(drag){
            //Сдвиг по горячим клавишам
				var x,y;
                if(drag.horizontal !== undefined){
                    x = glob.CURRENT.position().left + drag.horizontal
                }else{
                    x = glob.CURRENT.position().left;
                }

                if(drag.vertical !== undefined){
                    y = glob.CURRENT.position().top + drag.vertical;
                }else{
                    y = glob.CURRENT.position().top;
                }

                db.insert(glob.CURRENT_ID,'css',{'left':[x,'px'],'top':[y,'px']});
                view.draw();
                view.css();
                select.focus();
        }else{
			if(event.ctrlKey){
				activity.copy();
			}
            var shiftX = activity.clickX - glob.PAGE.find('.focus').position().left;
            var shiftY = activity.clickY - glob.PAGE.find('.focus').position().top;
			glob.BODY.find('#editor, .focus').css('cursor','move');

			var cacheFocus = document.getElementsByClassName('focus')[0].style;
			var cacheParent = document.getElementById('page');
			var cachePage = document.getElementById('page');

			var xElem, yElem = null; //Garbage Collector breaking
           	glob.BODY.on('mousemove', '#editor, .focus', function(event){

				var x = (event.pageX - shiftX)|0;
				var y = (event.pageY - shiftY)|0;

				window.requestAnimationFrame(function(){
					if(glob.SNAP_TO_GRID && !event.shiftKey){
						// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
						cacheFocus.left = (x/10|0)*10 + 'px';
						cacheFocus.top = (y/10|0)*10 + 'px';
						xElem = ((x - cacheParent.offsetLeft + cachePage.offsetLeft + 10)/10|0)*10;
						yElem = ((y - cacheParent.offsetTop + cachePage.offsetTop + 10)/10|0)*10;
					}else{
						cacheFocus.left = x + 'px';
						cacheFocus.top = y + 'px';
						xElem = x - cacheParent.offsetLeft + cachePage.offsetLeft + 10;
						yElem = y - cacheParent.offsetTop + cachePage.offsetTop + 10;
					}

					db.insert(glob.CURRENT_ID,'css',{'left':[xElem,'px'],'top':[yElem, 'px']});
					view.draw();
				});
            });

            glob.BODY.mouseup(function(){
                glob.BODY.off('mousemove');
				glob.EDITOR.find(".focus").css('cursor','default');
                view.css();
            });
        }
    },

    rotate:function(){
		glob.EDITOR.css('cursor','move');
        glob.EDITOR.mousemove(function(cursor){
			window.requestAnimationFrame(function(){
				var cx = cursor.pageX;
				var cy = cursor.pageY;

				var x = (glob.PAGE.find('.focus').offset().left)|0;
				var y = (glob.PAGE.find('.focus').offset().top)|0;
				var w = glob.PAGE.find('.focus').width();
				var h = glob.PAGE.find('.focus').height();
				var centerX = x + w / 2;
				var centerY = y + h / 2;

				var deg = -(Math.atan2(cx - centerX,cy - centerY) / Math.PI * 180 - 45)|0;

				db.insert(glob.CURRENT_ID,'css',{'rotate':[deg,'deg']});

				view.draw();
				view.css();
				formElems.write();
			});
        });

        glob.EDITOR.mouseup(function(){
            glob.EDITOR.off('mousemove');
			glob.EDITOR.css('cursor','default');
        })
    },

    resize:function(direction){
        var focusLeft = glob.PAGE.find('.focus').offset().left;
        var focusTop = glob.PAGE.find('.focus').offset().top;
        glob.EDITOR.mousemove(function(event){

            switch(direction){
                case 'top-left': glob.EDITOR.css('cursor','nw-resize'); break;
                case 'top-right': glob.EDITOR.css('cursor','ne-resize'); break;
                case 'bottom-left': glob.EDITOR.css('cursor','sw-resize'); break;
                case 'bottom-right': glob.EDITOR.css('cursor','se-resize'); break;
            }

            var cx = event.pageX;
            var cy = event.pageY;

			if(glob.SNAP_TO_GRID && !event.shiftKey){
				// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
				var width = ((cx - focusLeft - 20)/10|0)*10;
				var height = ((cy - focusTop - 20)/10|0)*10;
			}else{
				var width = cx - focusLeft - 20;
				var height = cy - focusTop - 20;
			}

			glob.PAGE.find('.focus').css({'width':width,'height':height});

            db.insert(glob.CURRENT_ID,'css',{'width':[width|0,'px'],'height':[height|0,'px']});
            view.draw();
        });

        glob.EDITOR.mouseup(function(){
            glob.EDITOR.off('mousemove');
			glob.EDITOR.css('cursor','default');
			view.css();
			formElems.write();
        })
    }
};
console.info('siteeditpro/activity');
var select = {

//    hover:function(element){ //$(this)
//		if(document.getElementsByClassName('overlay').length === 0){
//			$('body').append('<div class="overlay"></div>');
//			$('.overlay').data('id',glob.CURRENT_ID);
//		}
//
//		var width = element.outerWidth();
//		var height = element.outerHeight();
//		var rotate = element.css('transform');

//		var top = element.position().top-10;
//		var left = element.position().left-10;
//		$('.overlay').css({'width':width,'height':height,'top':top,'left':left,'-webkit-transform':rotate});
//    },
//
//    unhover:function(){
//        $('.overlay').remove();
//    },
	focusNum:0,
    focus:function(){
		var focusElems = (function(){
			var count;
			return count++;
		})();

        glob.CURRENT = glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]');

		var r = '<div class="drag rotate"></div>';
		var s = '<div class="drag bottom-right"></div>'; //'<div class="drag top-left"></div><div class="drag top-right"></div><div class="drag bottom-left"></div>'

		glob.PAGE.append('<div class="focus">' + s + r + '</div>');

        var width = glob.CURRENT.outerWidth();
        var height = glob.CURRENT.outerHeight();
        var rotate = db.select([glob.CURRENT_ID],'css',['rotate']);//glob.CURRENT.css('rotate');

		var top = (db.select([glob.CURRENT_ID],'css',['top'])-10)|0 + 'px';//(glob.CURRENT.position().top-10)|0;
		var left = (db.select([glob.CURRENT_ID],'css',['left'])-10)|0 + 'px'; //(glob.CURRENT.position().left-10)|0;
        glob.EDITOR.find('.focus').css({'width':width,'height':height,'top':top,'left':left,'transform':'rotate('+rotate+'deg)'});

        formElems.write();

        glob.CODE_PANEL.find('.css [data-id="' + glob.CURRENT_ID + '"]').addClass('hover');
        glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').addClass('hover');

        glob.TOP_VALUES_PANEL.find('.element-name h3').text(function(){
            return db.select([glob.CURRENT_ID],'props',['type']) + glob.CURRENT_ID;
        });
    },

    unfocus:function(){
        glob.CURRENT = '';
        glob.CURRENT_ID = '';

        glob.TOP_VALUES_PANEL.find('.element-name h3').text('body');
		glob.TOP_VALUES_PANEL.find('.properties').hide();
		glob.TOP_VALUES_PANEL.find('.body-tag').show();

		select.focusNum = 0;
        glob.PAGE.find('.drag, .focus').remove();
        glob.CODE_PANEL.find('.css *').removeClass('hover');
		glob.CODE_PANEL.find('.html *').removeClass('hover');
    },

	renewFocus:function(){
		glob.PAGE.find('.drag, .focus').remove();
		glob.CURRENT = glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]');


		var r = '<div class="drag rotate"></div>';
		var s = '<div class="drag bottom-right"></div>'; //'<div class="drag top-left"></div><div class="drag top-right"></div><div class="drag bottom-left"></div>'
		glob.PAGE.append('<div class="focus">' + s + r + '</div>');

		var width = glob.CURRENT.outerWidth();
		var height = glob.CURRENT.outerHeight();
		var rotate = db.select([glob.CURRENT_ID],'css',['rotate']);//glob.CURRENT.css('rotate');
		var top = (db.select([glob.CURRENT_ID],'css',['top'])-10)|0 + 'px';//(glob.CURRENT.position().top-10)|0;
		var left = (db.select([glob.CURRENT_ID],'css',['left'])-10)|0 + 'px'; //(glob.CURRENT.position().left-10)|0;

		glob.EDITOR.find('.focus').css({'width':width,'height':height,'top':top,'left':left, 'transform':'rotate('+rotate+'deg)'});
	}
};
console.info('siteeditpro/select');
var editor = {
	clickX:'',
	clickY:'',
	zoom:1,

	pull:function(element){
		var shiftX = editor.clickX - glob.PAGE.position().left - (glob.PAGE.width()/2|0) * (editor.zoom - 1);
		var shiftY = editor.clickY - glob.PAGE.position().top - (glob.PAGE.height()/2|0) * (editor.zoom - 1);

		glob.EDITOR.mousemove(function(event){
			window.requestAnimationFrame(function(){
				var x = (event.pageX - shiftX)|0;
				var y = (event.pageY - shiftY)|0;
				glob.PAGE.css({left:x,top:y});

				glob.EDITOR.find('.top-ruler-left').css('right',(glob.EDITOR.width() - glob.PAGE.offset().left)/editor.zoom - glob.CODE_PANEL.width() - 8);
				glob.EDITOR.find('.top-ruler-right').css('left',(glob.PAGE.offset().left - 41)/editor.zoom);
				glob.EDITOR.find('.left-ruler-top').css('bottom',(glob.EDITOR.height() - glob.PAGE.offset().top)*editor.zoom - 39);
				glob.EDITOR.find('.left-ruler-bottom').css('top',(glob.PAGE.offset().top - 140)/editor.zoom);
			});
		});

		$(document).mouseup(function(){
			glob.EDITOR.off('mousemove');
			element.css('cursor','url(/static/img/sprites/move.png),move');
		});
	},

	fullscreen:function(){
		if(document.webkitIsFullScreen){
			document.webkitCancelFullScreen()
		}else{
			document.getElementById('body').webkitRequestFullScreen()
		}
	},

	codePanelResize:function(){
		glob.BODY.mousemove(function(event){
			window.requestAnimationFrame(function(){
				var x = $(window).width() - event.pageX + 36;
				glob.CODE_PANEL.css('width',x);
				glob.EDITOR.css('width',$(window).width() - x - 40);
				glob.BODY.mouseup(function(){
					glob.BODY.off('mousemove');
				});
			});
		});
	},

	showPanel:function(panelName){
		glob.CODE_PANEL.find('.css, .html, .js').hide();
		switch(panelName){
			case 'css':glob.CODE_PANEL.find('.content .css').show(); break;
			case 'html':glob.CODE_PANEL.find('.content .html').show(); break;
			case 'js':glob.CODE_PANEL.find('.content .js').show(); break;
		}
	},

	hotKeys:function(event){
		switch(event.keyCode){
			case 13: activity.edit(); break; //ENTER Смена родительского объекта на текущий элемент
//			case 16: break; //SHIFT Выделение нескольких объектов
			//case 27: $('.darkness').remove(); glob.PARENT_ID = ''; break; //ESC Прекращение редактирования объекта
			case 37: event.preventDefault(); activity.drag('',{horizontal:-1}); break; //LEFT
			case 38: event.preventDefault(); activity.drag('',{vertical:-1}); break; //UP
			case 39: event.preventDefault(); activity.drag('',{horizontal:1}); break; //RIGHT
			case 40: event.preventDefault(); activity.drag('',{vertical:1}); break; //DOWN
			case 187: event.preventDefault(); editor.scale('5'); $(document).keyup(function(){editor.scale('0')}); break; //+
			case 189: event.preventDefault(); editor.scale('-3'); $(document).keyup(function(){editor.scale('0')}); break; //-
			case 46: event.preventDefault(); activity.del(); break; //DELETE
			case 122: event.preventDefault(); editor.fullscreen(); break; //F11
			case 186: event.preventDefault(); if(event.ctrlKey){$('#editor .horizontal-line, #editor .vertical-line, .light-point').toggle();} break; //CTRL + ;
			/* Shift+MouseClick выделить несколько объектов, Ctrl+MouseDrag dublicate, Ctrl+C Ctrl+Insert copy, Ctrl+V Shift+Insert paste, Ctrl+Z undo, Ctrl+Y redo */
		}
		//SHIFT + Click - Выделить несколько элементов
		//CTRL + Zoom - preventDefault, editor scale
		//+ (43) - мax zoom - На время удержания клавиши
		//- (45) - min zoom - На время удержания клавиши
	},

	scale:function(value){
		var scale = function(){
			var result;
			switch (value){
				case '-3': result = 0.25; glob.PAGE.css('background','#FFFFFF'); break;
				case '-2': result = 0.5; glob.PAGE.css('background','#FFFFFF'); break;
				case '-1': result = 0.7; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '0': result = 1; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '1': result = 1.5; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '2': result = 3; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '3': result = 5; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '4': result = 7; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
				case  '5': result = 10; glob.PAGE.css({'background':'url(/static/img/sprites/grid-100.png),#FFFFFF','background-size':'100px'}); break;
			}
			return result;
		};

		if(scale() !== 0){
			$('#page, #editor .focus, #editor .overlay, #editor .horizontal-line, #editor .vertical-line').css('-webkit-transform','scale(' + scale() + ')');
			glob.EDITOR.find('.top-ruler').css('-webkit-transform','scale(' + scale() + ',' + 1 + ')');
			glob.EDITOR.find('.left-ruler').css('-webkit-transform','scale(' + 1 + ',' + scale() + ')');
			editor.zoom = scale();
		}
	}
};
console.info('siteeditpro/editor');
var db = {
    dbdata:[],

    create:function(){
        var Elems = function(){
            //resize: true, false, proportional, vertical, horizontal
            this.props = [];
            this.css = [];
            this.html = [];
        };
        db.dbdata[db.dbdata.length] = new Elems();
		return db.dbdata[db.dbdata.length-1];
    },

	copy:function(currentId){
		var copy = library.clone(db.dbdata[currentId-1]);
		copy = _.extend(db.create(),copy);
		copy.props.id[0] = db.dbdata.length;
		return copy;
	},

    insert:function(elems,path,props){
        _.extend(db.dbdata[elems-1][path],props);
    },

    del:function(elems,type,props){
        if(props === undefined||''){
            db.dbdata[elems] = '';
        }else{
            db.dbdata[elems].type.props = '';
        }
    },

//Варианты использования:
    select:function(elems,path,props,info){ //[elems],dbMemPath,[props],1

        if(info === undefined||''){info = 0}

        for(var j = 0, k = elems.length-1; j <= k; j++){

			if (props === undefined || '') {	//возвращает объект - вывод кода
				var result = {};
				for (var prop in db.dbdata[elems - 1][path]) {
					var variable = db.dbdata[elems - 1][path][prop][info];
					if (variable !== undefined || '') {
						result[prop] = variable;
					}
				}
			} else {	//возвращает строку - получение одного заданного свойства
				var result = '';
				for (var i = 0, n = props.length - 1; i <= n; i++) {
					if (this.dbdata[elems - 1][path][props[i]] !== undefined || '') {
						var variable = db.dbdata[elems - 1][path][props[i]][info];
					}
					if (variable !== undefined || '') {
						result = result + variable;
					}
				}
			}

        }
        return result;
    }
};

var ls = {
	load:function(){

		//db.dbdata = JSON.parse(localStorage.getItem('db'));
		console.log(JSON.stringify(db.dbdata,"",4));
	},

	localSave:function(){
		localStorage.setItem('db',JSON.stringify(db.dbdata));
	},

	serverSave:function(){
		$.ajax({
			type: "POST",
			url: "ajax.php",
			dataType: 'JSON',

			data: {
				db: JSON.stringify(db.dbdata)
			},

			beforesend: $('#top .save-button').text('Идёт сохранение...'),

			success: function(data, code){
				if (code==200){
					$('.content').html(data); // запрос успешно прошел
				}else{
					$('.content').html(code); // возникла ошибка, возвращаем код ошибки
				}

				$('#top .save-button').text('Сохранено 3 минуты назад');
			},

			error:  function(xhr, str){
				$('#top .save-button').text('Проверьте подключение к Интернет');
			},

			complete: function(){}
		});
	}
};
console.info('siteeditpro/db');
var view = {//display
    draw:function(){
        //Не оптимальный код - вместо цикла нужно доработать db.select, чтобы можно было выбирать все эти данные за один запрос
            var str = {};
            var css = {};
            var k = '';
            for(var prop in db.select([glob.CURRENT_ID],'css')){
                //Костыль - сделать нормальное хранение сложных CSS в db + нормальный парсинг
                if(prop === 'rotate'){
                    str['transform'] = 'rotate(' + db.select([glob.CURRENT_ID],'css',[prop]) + db.select([glob.CURRENT_ID],'css',[prop],1) + ')';
                }else{
                    k = db.select([glob.CURRENT_ID],'css',[prop]) +  db.select([glob.CURRENT_ID],'css',[prop],1);
                    str[prop] = k;
                }
                _.extend(css,str);
            }
			glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]').css(css);
			var content = db.select([glob.CURRENT_ID],'props',['content']);
			glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]').html(content);
			var html = db.select([glob.CURRENT_ID],'html');
			glob.PAGE.find('[data-id="' + glob.CURRENT_ID + '"]').attr(html);
    },

    css:function(){
        var elementType = db.select([glob.CURRENT_ID],'props',['tag']);

        var result = '';
        for(var prop in db.select([glob.CURRENT_ID],'css')){
            if(db.select([glob.CURRENT_ID],'css',[prop]) !== undefined||''){
                result = result + '<li>' + prop + ':' + db.select([glob.CURRENT_ID],'css',[prop]) + db.select([glob.CURRENT_ID],'css',[prop],1) + '</li>';
            }
        }

        //Если элемент существует, мы его перезаписываем, а не добавляем
        if(glob.CODE_PANEL.find('.css [data-id="' + glob.CURRENT_ID + '"]').length > 0){
			glob.CODE_PANEL.find('.css [data-id="' + glob.CURRENT_ID + '"]').html('#' + db.select([glob.CURRENT_ID],'html',['id']) + '{<ul>'+ result +'</ul>}');
        }else{
			glob.CODE_PANEL.find('.css').append('<div data-id="' + glob.CURRENT_ID + '">#' + db.select([glob.CURRENT_ID],'html',['id']) + '{<ul>'+ result +'</ul>}</div>');
        }
    },

	html:function(){
		var deep = '';
		var parent = db.select([glob.CURRENT_ID],'props',['parent']);
		var elementType = db.select([glob.CURRENT_ID],'props',['tag']);
		var content = db.select([glob.CURRENT_ID],'props',['content']);

		var result = '';
		for(var prop in db.select([glob.CURRENT_ID],'html')){
			if(db.select([glob.CURRENT_ID],'html',[prop]) !== undefined||''){
				result = result + ' ' + prop + '="' + db.select([glob.CURRENT_ID],'html',[prop]) + '"';
			}
		}

//		var element = '&#60;' + elementType + result + '&#62;' + content + '&#60;/' + elementType + '&#62;';
		var element = '&#60;' + elementType + result + '&#62;' + _.escape(content) + '&#60;/' + elementType + '&#62;';

		//Если родитель элемента есть в наборе
		if(glob.CODE_PANEL.find('.html [data-id="' + parent + '"]').length){
			if(parent === undefined||''){
				glob.CODE_PANEL.find('.html').append('<div data-id="' + glob.CURRENT_ID + '">' + element + '</div>');
			}else{
				glob.CODE_PANEL.find('.html [data-id="' + parent + '"]').after('<div data-id="' + glob.CURRENT_ID + '">' + element + '</div>');
			}
			//Свигаем
			glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').css('margin-left',20*deep+'px');
		}else{
			//Обновляем существующий
			if(glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').length > 0){
				glob.CODE_PANEL.find('.html [data-id="' + glob.CURRENT_ID + '"]').html(element);
			}else{
				glob.CODE_PANEL.find('.html').append('<div data-id="' + glob.CURRENT_ID + '">' + element + '</div>');
			}
		}
	},

    code:function(){
        var head = 	'<!DOCTYPE html>\n' +
					'<head>\n' +
					'<meta charset="utf-8">\n' +
					'<link href="http://fonts.googleapis.com/css?family=Poiret+One|Philosopher|Yeseva+One|Tenor+Sans|Oranienbaum|Lobster|Cuprum|Open+Sans:800|Roboto:400,100|Roboto+Condensed:300,400|Forum|Prosto+One|Marmelad&subset=latin,cyrillic" rel="stylesheet" type="text/css">\n' +
					'<style>\n';
        var body = '</style>\n</head>\n<body style="background-color:#EEEEEE">\n<div style="position:relative; width:960px; height:1280px; margin:20px auto; background-color:#FFFFFF">\n';
        var end = '</div>\n</body>\n</html>';

        var prop = '';
        var css = '';
        var html = '';

        for(var j = 0, k = db.dbdata.length-1; j <= k; j++){

			for(var props in db.dbdata[j].css){
				var unit = '';
				if(db.dbdata[j].css[props][1] !== undefined||''){
					unit = db.dbdata[j].css[props][1]
				}
				prop = prop + '\n\t' + props + ':' + db.dbdata[j].css[props][0] + unit + ';';
			}

			css = css + '#' + db.dbdata[j].html['id'] + '{' + prop + '\n}\n';
			prop = '';

			for(var props in db.dbdata[j].html){
				prop = prop + ' ' + props + '="' + db.dbdata[j].html[props] + '"';
			}

			html = html + '\t<' + db.dbdata[j].props['tag'] + prop + '>' + db.dbdata[j].props['content'] + '</' + db.dbdata[j].props['tag'] + '>\n';
			prop = '';

		}

        var code = head + css + body + html + end;
        $('.popup .code').text(code);
    }
};
console.info('siteeditpro/view');
var formElems = {
    read:function(elem,value){ //$(this), $(this).val();
        var data = elem.data('value');
        var elInfo = data.split('_');

        var props = {};
        props[elInfo[2]] = [value,elInfo[3]];
        db.insert(glob.CURRENT_ID,elInfo[1],props);

        view.draw();
        view.css();
        view.html();
		select.renewFocus();
    },

    write:function(){   //<input type="number" data-value="p_css_width_px" />
        glob.TOP_VALUES_PANEL.find('.properties').hide();

        var elemType = db.select([glob.CURRENT_ID],'props',['type']);
        glob.TOP_VALUES_PANEL.find('.' + elemType + '-tag').show();

        glob.TOP_VALUES_PANEL.find('.' + elemType + '-tag input, .' + elemType + '-tag textarea').each(function(){
            var that = this;
            var value = (function(){
                var data = $(that).data('value');

				//Костыль - undefined - не вдаваясь в подробности - без него валятся ошибки и сбивается копирование по горячим клавишам
				if(data !== undefined){
					var elInfo = data.split('_');
					return db.select([glob.CURRENT_ID],elInfo[1],[elInfo[2]]);
				}

            })();
            $(this).val(value);
        });
		select.renewFocus();
    }
};
console.info('siteeditpro/formElems');
var elements = {
    checkbox:function(currentId){
        return '<input type="checkbox" id="" /><label for="">' + db.select([currentId],'props',['text']) + '</label>';
    },
    radio:function(currentId){
        return '<input type="radio" id="" /><label for="">' + db.select([currentId],'props',['text']) + '</label>';
    },
    slider:function(currentId){
        var slider = '\
\n<div class="slide"><img src="/application/pages/test/view/1.jpg" /></div>\
\n<div class="slide"><img src="/application/pages/test/view/2.jpg" /></div>\
\n<div class="slide"><img src="/application/pages/test/view/3.jpg" /></div>\
\n<div class="btn leftBtn">Влево</div>\
\n<div class="btn rightBtn">Вправо</div>\n';
        return slider;
    },
    map:function(currentId){
        return '<img src="//api-maps.yandex.ru/services/constructor/1.0/static/?sid=7umxXDqVPBREEyfDLVn2LvlXS500BmQ4&width=600&height=249" alt=""/>';
        //var map = '<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=7umxXDqVPBREEyfDLVn2LvlXS500BmQ4&width=600&height=249"></script>';
    },
    select:function(currentId){
        //select - запрет на поворот
        return '<select size="1"><option selected value="">Первый пункт</option><option value="">Второй пункт</option><option value="">Третий пункт</option><option value="">Четвёртый пункт</option></select>';
    }
};
console.info('siteeditpro/elements');
$(function(){
    $('#page .leftBtn').click(function(){slideLeft()});
    $('#page .rightBtn').click(function(){slideRight()});

    var slideRight = function(){
        $('#page .slide').first().before(function(){
            return $('.slide').last();
        });
    };

    var slideLeft = function(){
        $('#page .slide').last().after(function(){
            return $('#page .slide').first();
        });
    };

    setInterval(function(){
        slideLeft();
    },7000);
});
console.info('siteeditpro/append');
var guides = {
	addHorizontal:function(event,that){
		glob.EDITOR.css('cursor','row-resize');
		event.stopPropagation();
		that.mouseout(_.once(function(){
			var i = glob.EDITOR.find('.horizontal-line').length + 1;
			glob.EDITOR.append('<div class="horizontal-line numhorizontal' + i + '"><div></div></div>');
			glob.EDITOR.mousemove(function(event){

				if(event.shiftKey){
					glob.SNAP_TO_GRID = false;
				}else{
					glob.SNAP_TO_GRID = true;
				}

				if(glob.SNAP_TO_GRID){
					// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
					var y = (event.pageY/10|0)*10 - 141;
				}else{
					var y = event.pageY - 141;
				}

				glob.EDITOR.find('.horizontal-line.numhorizontal' + i).css('top', y);
				glob.EDITOR.find('.top-ruler').off('mousedown, mouseout');
				glob.EDITOR.mouseup(function(){
					glob.EDITOR.css('cursor','url("/static/img/sprites/move.png"),move');
					glob.EDITOR.off('mousemove');
				})
			})
		}))
	},

	addVertical:function(event,that){
		glob.EDITOR.css('cursor','col-resize');
		event.stopPropagation();
		that.mouseout(_.once(function(){
			var j = glob.EDITOR.find('.vertical-line').length + 1;
			glob.EDITOR.append('<div class="vertical-line numvertical' + j +'"><div></div></div>');
			glob.EDITOR.mousemove(function(event){

				if(event.shiftKey){
					glob.SNAP_TO_GRID = false;
				}else{
					glob.SNAP_TO_GRID = true;
				}

				if(glob.SNAP_TO_GRID){
					// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
					var x = (event.pageX/10|0)*10 - 46;
				}else{
					var x = event.pageX - 46;
				}

				glob.EDITOR.find('.vertical-line.numvertical' + j).css('left',x);
				glob.EDITOR.find('.left-ruler').off('mousedown, mouseout');
				glob.EDITOR.mouseup(function(){
					glob.EDITOR.css('cursor','url("/static/img/sprites/move.png"),move');
					glob.EDITOR.off('mousemove');
				})
			});
		}));
	},

	moveHorizontal:function(event,that){
		glob.EDITOR.css('cursor','row-resize');
		event.stopPropagation();
		glob.EDITOR.mousemove(function(event){

			if(event.shiftKey){
				glob.SNAP_TO_GRID = false;
			}else{
				glob.SNAP_TO_GRID = true;
			}

			if(glob.SNAP_TO_GRID){
				// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
				var y = (event.pageY/10|0)*10 - 141;
			}else{
				var y = event.pageY - 141;
			}

			that.css('top',y);

			glob.BODY.mouseup(function(){
				glob.EDITOR.off('mousemove');
				if(that.position().top < 16){
					that.remove();
				}
			});
		});
	},

	moveVertical:function(event,that){
		glob.EDITOR.css('cursor','col-resize');
		event.stopPropagation();
		glob.EDITOR.mousemove(function(event){

			if(event.shiftKey){
				glob.SNAP_TO_GRID = false;
			}else{
				glob.SNAP_TO_GRID = true;
			}

			if(glob.SNAP_TO_GRID){
				// (...)/10|0)*10 - рассчёт привязки к десятепиксельной сетке
				var x = (event.pageX/10|0)*10 - 46;
			}else{
				var x = event.pageX - 46;
			}

			that.css('left',x);

			glob.BODY.mouseup(function(){
				glob.EDITOR.off('mousemove');
				if(that.position().left < 16){
					that.remove();
				}
			});
		});
	}
};
console.info('siteeditpro/guides');
var siteEditProEvents = {
	events:function(){
		//Событие изменения размера окна браузера -------------
			var codePanelHeight = $(window).height()-171;
			glob.CODE_PANEL.css('height',codePanelHeight);

			$(window).resize(function(){
				codePanelHeight = $(window).height()-171;
				glob.CODE_PANEL.css('height',codePanelHeight);
				if(glob.CURRENT){
					var rotate = db.select([glob.CURRENT_ID],'css',['transform']);
					var x = glob.CURRENT.position().left - 12;
					glob.EDITOR.find('.focus').css({left:x});
				}
			});
		//-----------------------------------------------------


		//Горячие клавиши -------------------------------------
			$(document).on('keydown',function(event){editor.hotKeys(event);});
		//-----------------------------------------------------


		//Выделение элемента ----------------------------------
			glob.CODE_PANEL.find('.css, .html').on('click','div',function(){select.unfocus(); glob.CURRENT_ID = $(this).data('id'); select.focus();});
			glob.BODY.on('mousedown','#editor',function(){select.unfocus();});
		//-----------------------------------------------------





		//Изменение параметров элемента из верхней панели -----
			glob.BODY.on('change','#snap-to-grid',function(){
				if($(this).prop('checked')){
					glob.SNAP_TO_GRID = true;
				}else{
					glob.SNAP_TO_GRID = false;
				}
			});

			glob.TOP_VALUES_PANEL.on('change','[data-value="header_css_font-size_px"]',function(){
				var that = $(this);
				if($('#header-font-line').prop('checked')){
					$('[data-value="header_css_line-height_px"]').val(function(){return that.val()}).change();
				}
			});

//			glob.TOP_VALUES_PANEL.on('change','[data-value="header_css_line-height_px"]',function(){
//				var that = $(this);
//				if($('#header-font-line').prop('checked')){
//					$('[data-value="header_css_font-size_px"]').val(that.val()).change();
//				}
//			});

			glob.TOP_VALUES_PANEL.on('change','[data-value="p_css_font-size_px"]',function(){
				var that = $(this);
				if($('#p-font-line').prop('checked')){
					$('[data-value="p_css_line-height_px"]').val(function(){return Math.ceil(that.val()*1.6)}).change();
				}
			});

			glob.BODY.on('change','input[type="number"], input[type="text"], textarea, select, input[type="color"]',function(){formElems.read($(this),$(this).val());});
			glob.BODY.on('keyup','input[type="number"], input[type="text"], textarea, select, input[type="color"]',function(){formElems.read($(this),$(this).val());});
			glob.BODY.on('click','.background',function(){formElems.read($(this),$(this).css('background'))});
		//-----------------------------------------------------


		//Редактирование элемента -----------------------------
//			glob.EDITOR.on('dblclick','.focus',function(){activity.edit();});
//			glob.EDITOR.on('dblclick','.darkness',function(){activity.stopEdit();});
		//-----------------------------------------------------


		//Перетаскивание рабочей области ----------------------
			glob.EDITOR.mousedown(function(event){$(this).css('cursor','url(/static/img/sprites/drag.png),move'); editor.clickX = event.pageX; editor.clickY = event.pageY; editor.pull($(this));});
		//-----------------------------------------------------


		//Перетаскивание элемента -----------------------------
			glob.PAGE.on('mousedown','.dragable',function(event){ event.stopPropagation(); event.preventDefault(); activity.clickX = event.pageX; activity.clickY = event.pageY; select.unfocus(); glob.CURRENT = $(this); glob.CURRENT_ID = $(this).data('id'); select.focus(); drag.call(this,event);});
			glob.PAGE.on('mousedown','.focus',function(event){event.stopPropagation(); activity.clickX = event.pageX; activity.clickY = event.pageY; activity.drag(event);});
			function drag(event){event.stopPropagation(); activity.clickX = event.pageX; activity.clickY = event.pageY; activity.drag(event);}
		//-----------------------------------------------------


		//Изменение размера элемента --------------------------
			glob.PAGE.on('mousedown','.top-left',function(event){event.stopPropagation(); activity.resize('top-left');});
			glob.PAGE.on('mousedown','.top-right',function(event){event.stopPropagation(); activity.resize('top-right');});
			glob.PAGE.on('mousedown','.bottom-left',function(event){event.stopPropagation(); activity.resize('bottom-left');});
			glob.PAGE.on('mousedown','.bottom-right',function(event){event.stopPropagation(); activity.resize('bottom-right');});
		//-----------------------------------------------------


		//Поворот элемента ------------------------------------
			glob.PAGE.on('mousedown','.rotate',function(event){event.stopPropagation(); activity.rotate();});
		//-----------------------------------------------------


		//Изменение размера боковой панели --------------------
			glob.CODE_PANEL.find('.resize-code-panel').mousedown(function(){editor.codePanelResize();});
		//-----------------------------------------------------


		//Направляющие ----------------------------------------
			glob.EDITOR.find('.top-ruler').mousedown(function(event){guides.addHorizontal(event,$(this));});
			glob.EDITOR.find('.left-ruler').mousedown(function(event){guides.addVertical(event,$(this));});
			glob.EDITOR.find('.ruler-button').mousedown(function(event){guides.addHorizontal(event,$(this)); guides.addVertical(event,$(this));});
			glob.EDITOR.on('mousedown','.horizontal-line',function(event){guides.moveHorizontal(event,$(this));});
			glob.EDITOR.on('mousedown','.vertical-line',function(event){guides.moveVertical(event,$(this));});
		//-----------------------------------------------------

		//Переименование элемента -----------------------------
			$('#element-name').change(function(){activity.rename($(this).val());});
			$('#element-name').keyup(function(){activity.rename($(this).val());});
		//-----------------------------------------------------


		//Зум -------------------------------------------------
			$('#editor-scale').change(function(){editor.scale($(this).val());});
		//-----------------------------------------------------


//		//Вывод координат курсора внизу страницы --------------
//			var xPos = document.getElementById('cursorPageX');
//			var yPos = document.getElementById('cursorPageY');
//			glob.EDITOR.mousemove(function(event){xPos.innerText = event.pageX; yPos.innerText = event.pageY});
//		//-----------------------------------------------------



		glob.BODY.on('click','.close-button',function(){codePopup.hide($(this));});

		glob.BODY.on('change','.files',function(){activity.addImage(event);});
		glob.BODY.on('click','.video-apply',function(){activity.addVideo($('.popup .video').val());});

//		//Выбор borders для настройки div
//		var BORDERS = $('#top-values-panel .div-tag .borders');
//		BORDERS.find('input').change(function(){
//			if(BORDERS.find('.top').prop('checked')){BORDERS.css('border-top-color','#B9BEC3');}else{BORDERS.css('border-top-color','#EEEEEE');}
//			if(BORDERS.find('.right').prop('checked')){BORDERS.css('border-right-color','#B9BEC3');}else{BORDERS.css('border-right-color','#EEEEEE');}
//			if(BORDERS.find('.bottom').prop('checked')){BORDERS.css('border-bottom-color','#B9BEC3');}else{BORDERS.css('border-bottom-color','#EEEEEE');}
//			if(BORDERS.find('.left').prop('checked')){BORDERS.css('border-left-color','#B9BEC3');}else{BORDERS.css('border-left-color','#EEEEEE');}
//		});

//		$('#code-panel').on('click','.html div',function(){$(this).DmSyntaxXML()});
//		$('textarea').on('click','.code',function(){$(this).DmSyntaxHTML()});
		$('#code-panel').find('.result-button').click(function(){codePopup.show($(this)); view.code();});
		$('.shadow-editor-show').click(function(){shadowEditorPopup.show($(this));});
		glob.BODY.on('click','.color-picker-show',function(){colorEditorPopup.show($(this));});


		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//Почему отсюда вызывается нормально один раз, а из popupEvents - 2??????????????????????????????????????
		glob.BODY.on('click','.add-light-point',function(){shadowEditorPopup.addLightPoint();});
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		introductionPopup.show();
//		setInterval(function(){ls.load()},3000);
	}
};
siteEditProEvents.events();

console.info('siteeditpro/siteEditProEvents');