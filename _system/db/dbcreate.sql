CREATE SCHEMA `db` DEFAULT CHARACTER SET utf8 ;

CREATE  TABLE `db`.`users` (
  `u_id_user` INT NOT NULL ,
  `u_name` VARCHAR(45) NULL ,
  `u_patronymic` VARCHAR(45) NULL ,
  `u_lastname` VARCHAR(45) NULL ,
  `u_birthday` DATE NULL ,
  `u_email` VARCHAR(255) NULL ,
  `u_password` VARCHAR(255) NULL ,
  PRIMARY KEY (`u_id_user`) )
DEFAULT CHARACTER SET = utf8;

CREATE  TABLE `db`.`bugtracker` (
  `bug_id` INT NOT NULL AUTO_INCREMENT ,
  `bug_id_user` VARCHAR(45) NULL ,
  `bug_info` TINYTEXT NULL ,
  `bug_vote` INT NULL ,
  `bug_browser` VARCHAR(45) NULL ,
  PRIMARY KEY (`bug_id`) );