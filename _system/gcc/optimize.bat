﻿cd ../..
cd static/js/siteeditpro
java -jar compiler.jar --js activity.js --js db.js --js editor.js --js elements.js --js glob.js --js popup.js --js formElems.js --js select.js --js view.js --compilation_level ADVANCED_OPTIMIZATIONS --externs externs.js --js_output_file siteeditpro.js --charset utf-8 --language_in ECMASCRIPT5 --create_source_map script-min.js.map --source_map_format=V3 --help
pause